#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <assert.h>

#include "Octree.h"
#include "DataStructure.h"

#undef DEBUG_SEPARATION

using namespace std;
extern int trackingGroupIndex;
extern int trackingGroupId;
extern double trackPos[3];

Group**	Group::ppBaseGroup = NULL;
Group*	Group::reserved[GROUP_RESERVE];

int GroupEntity::last_id = 0;
int Group::last_id = 0;
int Group::groupCount = 0;
int Group::array_size = 10000;
GroupDump *GroupDump::buffer = NULL;

GroupEntity::GroupEntity(void)
{
	id = 0;
	pPrev = pNext = NULL;
	pPointEntry = NULL;
}

GroupEntity::GroupEntity(class PointEntry *point)
{
	id = ++last_id;
	pPrev = pNext = NULL;
	pPointEntry = point;
}

GroupEntity::GroupEntity(GroupEntity *src)
{
	assert(0);
}

GroupEntity::~GroupEntity(void)
{
//	fprintf(stderr,"Delete Group Entity %d %p %p\n", id, pNext, pPointEntry);
	if ( pPointEntry )
	{
		pPointEntry->grp			= NULL;
		pPointEntry->pGroupEntity	= NULL;
	}
}

Group::Group(void)
{
	id = ++last_id;

	init();
}

Group::Group(int _id)
{

	id = _id;

	init();
}

void Group::init(void)
{
	int i;

	groupCount++;

	firstEntity	= new GroupEntity();
	lastEntity	= new GroupEntity();
	firstEntity->pNext	= lastEntity;
	lastEntity->pPrev	= firstEntity;
	numPoint = 0;

	for ( i = 0 ; i < 3 ; i++ )
	{
		pos[i]		= 0.;
		var[i]		= 0.;
		pos_p[i]	= 0.;
		var_p[i]	= 0.;
		pos_sum[i]	= 0.;
		pos_ssum[i]	= 0.;;
	}

	valid	= true;
	sorted	= false;
#ifdef GROUP_LIST
	pPrev	= NULL;
	pNext	= NULL;
#endif
}


// Group is copied only when it was checked
// DO NOT USE!!!
Group::Group(Group *src)
{
	assert(src->valid);

	id			= src->id;
	index		= src->index;
	valid		= src->valid;
	numPoint	= src->numPoint;
	sorted		= src->sorted;

	for ( int i = 0 ; i < 3 ; i++ )
	{
		pos[i]		= 0.;
		var[i]		= 0.;
		pos_sum[i]	= 0.;
		pos_ssum[i]	= 0.;;
	}

	firstEntity	= new GroupEntity();
	lastEntity	= new GroupEntity();
	firstEntity->pNext	= lastEntity;
	lastEntity->pPrev	= firstEntity;

	GroupEntity *it = src->firstEntity->pNext;

	GroupEntity *dst = firstEntity;
	while ( it != lastEntity)
	{
		GroupEntity *entity = new GroupEntity(it);

		dst->pNext		=  entity;
		entity->pPrev	= dst;

		it = it->pNext;
	}
}

Group::~Group()
{
	assert(valid);
	groupCount--;
	valid = false;

//	fprintf(stderr,"Delete Group %d %p\n", id, pEntity);

	GroupEntity *it = firstEntity;
	while ( it )
	{
		GroupEntity *delGroupEntity;
		
		delGroupEntity = it;
		it = it->pNext;
	
		delete delGroupEntity;
	}

	assert(ppBaseGroup[index] == this );
	ppBaseGroup[index] = NULL;
}

void Group::initialize(void)
{
	assert(!ppBaseGroup);
	ppBaseGroup = (Group **)malloc(sizeof(Group*)*Group::array_size);
	for ( int i = 0 ; i < Group::array_size ; i++ )
	{
		ppBaseGroup[i] = NULL;
	}
	Group::last_id		= GROUP_RESERVE;
	Group::groupCount	= 0;

	reserved[0] = new Group(0);
	reserved[0]->index = 0;
	
	return;
}

void Group::log(void)
{
	fprintf(stderr, "Group Count: %d\n", groupCount);
}

void Group::reset(void)
{
	for ( int i = 0 ; i < Group::array_size ; i++ )
	{
		if ( ppBaseGroup[i] )
		{
			delete ppBaseGroup[i];
		}
	}

	last_id		= GROUP_RESERVE;
	groupCount	= 0;
}


Group * Group::create(void)
{
	Group *group;

	group = new Group();	

	int i;

	for ( i = 0 ; i < array_size ; i++ )
	{
		if ( !ppBaseGroup[i] )
			break;
	}
	// Array overflow
	if ( i >= array_size )
	{
		fprintf(stderr, "Group Array Overflow!!!\n");
		assert(0);
	}
	ppBaseGroup[i] = group;
	group->index = i;

	return group;
}

Group *Group::merge(Group *grp1, Group *grp2)
{
	Group *group, *delGroup;

	if ( grp1->id == grp2->id )
	{
		assert(grp1 == grp2);
		return grp1;
	}

	if ( grp1->numPoint >= grp2->numPoint )
	{
		group = grp1;
		delGroup = grp2;
	}
	else
	{
		group = grp2;
		delGroup = grp1;
	}
//	fprintf(stderr, "Merge %d %d (%p %p) (%d %d)\n", grp1->id, grp2->id, grp1, grp2, grp1->numPoint, grp2->numPoint);


	// Change the group of the points in the deleting group
	GroupEntity *entity = delGroup->firstEntity->pNext;
	GroupEntity *last	= delGroup->lastEntity;
	while ( entity != last )
	{
		assert(entity != entity->pNext);
		entity->pPointEntry->grp = group;
		entity = entity->pNext;
	}

	// Merge two Group entities
#if 1
	delGroup->firstEntity->pNext->pPrev = group->lastEntity->pPrev;
	delGroup->lastEntity->pPrev->pNext = group->lastEntity;
	group->lastEntity->pPrev->pNext = delGroup->firstEntity->pNext;
	group->lastEntity->pPrev = delGroup->lastEntity->pPrev;
	assert ( delGroup->firstEntity );
	delGroup->firstEntity->pNext = delGroup->lastEntity;
	delGroup->lastEntity->pPrev = delGroup->firstEntity;
#else
	GroupEntity *list1, *list2;
	GroupEntity *merged;

	list1 = group->pEntity->pNext;
	list2 = delGroup->pEntity->pNext;
merged = group->pEntity;

	while ( list1 || list2 )
	{
		GroupEntity *chosen;

		if ( list1 && list2 )
		{
			if ( list1->pPointEntry->tree->x < list2->pPointEntry->tree->x )
			{
				chosen = list1;
				list1 = list1->pNext;
			}
			else
			{
				chosen = list2;
				list2 = list2->pNext;
			}

			merged->pNext = chosen;
			chosen->pPrev = merged;
			chosen->pNext = NULL;
			merged = chosen;
		}
		else
		{
			if ( list1 )
				chosen = list1;
			else
				chosen = list2;

			merged->pNext = chosen;
			chosen->pPrev = merged;
			break;
		}
	}

#endif
	delGroup->firstEntity->pNext = delGroup->lastEntity;
	delGroup->lastEntity->pPrev = delGroup->firstEntity;

	for ( int i = 0 ; i < 3 ; i++ )
	{
		group->pos_sum[i]	+= delGroup->pos_sum[i];
		group->pos_ssum[i]	+= delGroup->pos_ssum[i];
	}
	group->numPoint += delGroup->numPoint;
	group->setDirty();
	
#ifdef GROUP_LIST
#if 1
	assert(delGroup->pPrev);
	delGroup->pPrev->pNext = delGroup->pNext;
	if ( delGroup->pNext )
		delGroup->pNext->pPrev = delGroup->pPrev;
#endif
#endif

	if ( delGroup->index == trackingGroupIndex && delGroup->id == trackingGroupId )
	{
		trackingGroupIndex	= group->index;
		trackingGroupId		= group->id;
	}

	delete delGroup;

	group->sorted = false;

	return group;
}

void Group::addPoint(PointEntry *entry)
{
	GroupEntity *newEntry = new GroupEntity(entry);
	entry->grp = this;

	assert(firstEntity);
//	if ( pEntity )
	{
#if 0
		GroupEntity *last = pEntity;

		while ( last->pNext )
		{
			last = last->pNext;
		}
		if ( newEntry == last )
		{
			fprintf(stderr, "Adding existing entry %p\n", (void*)entry);
		}
		else
		{
			last->pNext = newEntry;
			newEntry->pPrev = last;
		}
#else
#if 1
		newEntry->pNext = firstEntity->pNext;
		newEntry->pPrev = firstEntity;
		firstEntity->pNext = newEntry;
		newEntry->pNext->pPrev = newEntry;
#else
		GroupEntity *prev = pEntity;


		if ( prev )
		{
			double x = newEntry->pPointEntry->x;

			while ( prev->pNext && prev->pNext->pPointEntry->x < x )
			{
				prev = prev->pNext;
			}

			assert( newEntry != prev );

			newEntry->pNext = prev->pNext;
			newEntry->pPrev = prev;
			if ( prev->pNext )
				prev->pNext->pPrev = newEntry;
			prev->pNext = newEntry;
		}
		else
		{
			pEntity->pNext = newEntry;
			newEntry->pPrev = pEntity;
		}
#endif
#endif
		double x, y, z;
		x = entry->x, y = entry->y, z = entry->z;
		pos_sum[0]	+=  x;
		pos_sum[1]	+=  y;
		pos_sum[2]	+=  z;
		pos_ssum[0]	+=  x*x;
		pos_ssum[1]	+=  y*y;
		pos_ssum[2]	+=  z*z;
		numPoint++;

		setDirty();

	}
//	else
//	{
//		pEntity = newEntry;
//	}

	sorted = false;

	entry->pGroupEntity	= newEntry;
	entry->grp			= this;
}

void Group::removePoint(PointEntry *entry)
{
//	assert(this == entry->grp);
	Group	*group = entry->grp;
	GroupEntity *pGroupEntity = entry->pGroupEntity;

	assert( pGroupEntity );

	pGroupEntity->pPrev->pNext = pGroupEntity->pNext;
	pGroupEntity->pNext->pPrev = pGroupEntity->pPrev;
	pGroupEntity->pPrev = pGroupEntity->pNext = NULL;

	delete pGroupEntity;

	double x, y, z;
	x = entry->x, y = entry->y, z = entry->z;
	group->pos_sum[0]	-=  x;
	group->pos_sum[1]	-=  y;
	group->pos_sum[2]	-=  z;
	group->pos_ssum[0]	-=  x*x;
	group->pos_ssum[1]	-=  y*y;
	group->pos_ssum[2]	-=  z*z;
	group->numPoint--;
	group->setDirty();

#if 1
	if ( group->id > GROUP_RESERVE && group->numPoint == 0 )
	{
		assert(group->firstEntity->pNext == group->lastEntity && group->lastEntity->pPrev == group->firstEntity );
		delete group;
	}
#endif

}

void Group::sort(void)
{
	GroupEntity *bucket;

	if ( sorted )
		return;

	if ( numPoint <= 1 )
	{
		sorted = true;
		return;
	}

	bucket = firstEntity->pNext;
	bucket->pPrev = NULL;
	lastEntity->pPrev->pNext = NULL;

	assert(bucket);

	firstEntity->pNext	= lastEntity;
	lastEntity->pPrev	= firstEntity;

	while ( bucket )
	{
		GroupEntity *list	= firstEntity;
		GroupEntity *piece	= bucket;
		bucket = bucket->pNext;

		double x = piece->pPointEntry->tree->x;

		while ( list->pNext != lastEntity && list->pNext->pPointEntry->tree->x <= x )
		{
			list = list->pNext;
		}
		
		piece->pPrev = list;
		piece->pNext = list->pNext;

		list->pNext->pPrev	= piece;
		list->pNext			= piece;

	}
	sorted = true;
}

void Group::stat(void)
{
	int i;

	dirty = false;

	if ( numPoint == 0 )
		return;
	
#if 1
	for ( i = 0 ; i < 3 ; i++ )
	{
		pos_sum[i] = 0.;
		pos_ssum[i] = 0.;
	}
	int count = 0;
	GroupEntity *entity = firstEntity->pNext;
	while ( entity != lastEntity )
	{
		double p[3];
		PointEntry *point = entity->pPointEntry;

		p[0] = point->x;
		p[1] = point->y;
		p[2] = point->z;

		for ( i = 0 ; i < 3 ; i++ )
		{
			pos_sum[i]	+= p[i];
			pos_ssum[i]	+= p[i]*p[i];
		}
		count++;
		entity = entity->pNext;
	}
	assert(count == numPoint);
#endif

	for ( i = 0 ; i < 3 ; i++ )
	{
		pos[i] = pos_sum[i] / numPoint;
		var[i] = pos_ssum[i] / numPoint - pos[i]*pos[i];
	}
}

bool Group::check(void)
{
	bool ret = true;
	int i;

	GroupEntity *entity;

	entity = firstEntity;
//	if ( numPoint )
	{
		entity = firstEntity->pNext;
		for ( i = 0 ; i < numPoint ; i++ )
		{
			entity = entity->pNext;
		}
//		assert (entity == lastEntity);
		if (entity != lastEntity)
			*(int*)0 = 0;

		entity = lastEntity->pPrev;
		for ( i = 0 ; i < numPoint ; i++ )
		{
			entity = entity->pPrev;
		}
//		assert (entity == firstEntity);
		if (entity != firstEntity)
			*(int*)0 = 0;


	}

	assert(id < GROUP_RESERVE || this == ppBaseGroup[index]);

	return ret;
}

bool Group::isBase(void)
{
	bool ret = false;
	double x, y, z;

	if ( id == 0 )
		return true;
	else
		return false;

	x = pos[0];
	y = pos[1];
	z = pos[2];

	// Condition for Base itself
	if ( x*x + y*y < .7*.7 && ((z+.2)*(z+.2) < .1*.1 || (z < -0.18)) )
		ret = true;

	return ret;
}

double	high_z;
double	farthest;
double	avg[2] = {0., 0.};
double	_center;
double	min_rad, max_rad;
double	min_pos[2], max_pos[2];
bool Group::isBase(PointEntry *entry)
{
	double x, y, z;
	assert(entry);

	x = entry->x, y = entry->y, z = entry->z;

	if ( z > high_z )
		return false;
	if ( (x*x + y*y) > farthest * farthest )
		return false;

	if ( (min_pos[0]*y - min_pos[1]*x < 0) || 
		 (x*max_pos[1] - y*max_pos[0] < 0) ) 
		return false;

	return true;
}

void Group::defineBase(void)
{
	Group *base = reserved[0];

	high_z = -2.;
	farthest = 0.;

	int		count = 0;

	GroupEntity *it = base->firstEntity->pNext;	
	while ( it != base->lastEntity )
	{
		PointEntry *entry = it->pPointEntry;
		double dist_xy;
		double x, y, z;

		x = entry->x * 1.1;
		y = entry->y * 1.1;
		z = entry->z + 0.1;

		if ( z > high_z )
			high_z = z;

		dist_xy = x*x + y*y;

		if ( dist_xy > farthest )
			farthest = dist_xy;

		avg[0] += x;
		avg[1] += y;

		it = it->pNext;
		count++;
	}

	farthest = sqrt(farthest);

	avg[0] = avg[0] / count;
	avg[1] = avg[1] / count;

	double _center = atan2(avg[1], avg[0]);


	it = base->firstEntity->pNext;	
	min_rad = 1., max_rad = -1.;
	min_pos[0] = min_pos[1] = max_pos[0] = max_pos[1] = 0.;
	while ( it != base->lastEntity )
	{
		PointEntry *entry = it->pPointEntry;
		double x, y;

		x = entry->x, y = entry->y;

		double cp = (avg[0] * y - avg[1] * x) / sqrt(x*x + y*y);

		if ( cp > max_rad )
		{
			max_rad = cp;
			max_pos[0] = x;
			max_pos[1] = y;
		}

		if ( cp < min_rad )
		{
			min_rad = cp;
			min_pos[0] = x;
			min_pos[1] = y;
		}

		it = it->pNext;
	}

	fprintf(stderr, "Center (%f,%f) %f rad (%f ~ %f rad)\n", avg[0], avg[1], _center, min_rad, max_rad);
}

double GroupDumpEntity::getDistance2(GroupDumpEntity *entity1, GroupDumpEntity *entity2)
{
	double xx, yy, zz;

	xx = entity1->x - entity2->x;
	yy = entity1->y - entity2->y;
	zz = entity1->z - entity2->z;

	return (xx*xx + yy*yy + zz*zz);
}

GroupDump::GroupDump(Group *src)
{
	int i;

	groupId		= src->id;
	groupIndex	= src->index;
	numPoint	= src->numPoint;
	sorted		= src->sorted;
	seperated	= false;
	numGroup	= 0;

	list		= (GroupDumpEntity *)malloc(sizeof(GroupDumpEntity)*numPoint);
	sortedList	= (GroupDumpEntity **)malloc(sizeof(GroupDumpEntity*)*numPoint);

	GroupEntity *entity = src->firstEntity->pNext;

//	fprintf(stderr, "\n");
	double prevX = -100000.;
	for ( i = 0 ; i < numPoint ; i++ )
	{
		Octree *tree;
		assert(entity && entity->pPointEntry);
		tree = entity->pPointEntry->tree;
		
		list[i].id = entity->id;
		list[i].x = tree->x;
		list[i].y = tree->y;
		list[i].z = tree->z;
		list[i].tag = 0;

		sortedList[i] = &list[i];

		entity = entity->pNext;

//		fprintf(stderr, "%f ", list[i].x);
		assert( !sorted || prevX <= list[i].x );
		prevX = list[i].x;
	}
	tag_mask = NULL;
//	fprintf(stderr, "\n");
}

GroupDump::~GroupDump(void)
{
	free(list);
	free(sortedList);

	if (tag_mask)
		free(tag_mask);
}

void GroupDump::swap(GroupDumpEntity *entity1, GroupDumpEntity *entity2)
{
	GroupDumpEntity swap;

	memcpy((void *)&swap, (void *)entity1, sizeof(GroupDumpEntity));
	memcpy((void *)entity1, (void *)entity2, sizeof(GroupDumpEntity));
	memcpy((void *)entity2, (void *)&swap, sizeof(GroupDumpEntity));
}

void GroupDump::sort(void)
{
	int i, j;

	if ( numPoint <= 1 )
	{
		sorted = true;
		return;
	}

	for ( i = 0 ; i < numPoint-1 ; i++ )
	{
		for ( j = 0 ; j < numPoint-1-i ; j++ )
		{
			GroupDumpEntity *entity1 = sortedList[j];
			GroupDumpEntity *entity2 = sortedList[j+1];

			if ( entity1->x >= entity2->x )
			{
				sortedList[j] = entity2;
				sortedList[j+1] = entity1;
			}
		}
	}
	sorted = true;
}

bool GroupDump::check(void)
{
	int i;

	double prevX = -100000.;
	for ( i = 0 ; i < numPoint ; i++ )
	{
		assert(sortedList[i]);

		assert(sortedList[i]->x >= prevX);
		prevX = sortedList[i]->x;
	}
}

int GroupDump::seperate(double threshold )
{
	int i, j;
	int tag;
	bool changed;
	int trial;
	double threshold2 = threshold * threshold;

	tag = 1;
	sortedList[0]->tag = 1;

	trial = 0;
	changed = true;
	while ( changed &&  trial < 10 )
	{
		changed = false;
		for ( i = 0 ; i < numPoint ; i++ )
		{
			GroupDumpEntity *entity1 = sortedList[i];
			int tag1 = entity1->tag;

			if ( tag1 == 0 )
				tag1 = ++tag;
			for ( j = i-1 ; j >= 0 ; j-- )
			{
				GroupDumpEntity *entity2 = sortedList[j];
				int tag2 = entity2->tag;

				if ( fabs( entity1->x - entity2->x ) > threshold )
					break;

				assert( entity2->x <= entity1->x );
				if ( (GroupDumpEntity::getDistance2(entity1, entity2) < threshold2) && (tag1 != tag2) )
				{
					if ( tag2 && tag1 > tag2 )
						tag1 = tag2;
					else
						tag2 = tag1;
					changed = true;
				}
				entity2->tag = tag2;
			}
			for ( j = i+1 ; j < numPoint ; j++ )
			{
				GroupDumpEntity *entity2 = sortedList[j];
				int tag2 = entity2->tag;

				if ( fabs( entity1->x - entity2->x ) > threshold )
					break;

				assert( entity2->x >= entity1->x );
				if ( (GroupDumpEntity::getDistance2(entity1, entity2) < threshold2) && (tag1 != tag2) )
				{
					if ( tag2 && tag1 > tag2 )
						tag1 = tag2;
					else
						tag2 = tag1;
					changed = true;
				}
				entity2->tag = tag2;
			}
			entity1->tag = tag1;
		}
		trial++;
	}
//	fprintf(stderr, "MAX Tag: %d\n", tag);

	max_tag = 0;
	int tag_boundary = tag+1;
	tag_mask = (unsigned int *)calloc(sizeof(unsigned int),max((tag_boundary>>5) + 2, 8));
	for ( i = 0 ; i < numPoint ; i++ )
	{
		int tag = list[i].tag;
//		fprintf(stderr, "%f:%d ", list[i].x, tag);
		if ( max_tag < tag )
			max_tag = tag;

		if ( tag >= tag_boundary )
		{
			fprintf(stderr, "Group Tag overflowed %d >= %d\n", tag, tag_boundary);
			assert(0);
		}

		tag_mask[tag>>5] |= 1<<(tag&0x1F);
	}
//	fprintf(stderr, "\n");
	int num_group = 0;
	for ( i = 0 ; i < (max_tag>>5) + 1 ; i++ )
	{
		unsigned int mask = tag_mask[i];
//		fprintf(stderr, "%08x ", mask);
		for ( j = 0 ; j < 32 && mask ; j++ )
		{
			if ( mask & 1 )
				num_group++;

			mask = mask >> 1;
		}
	}
//	fprintf(stderr, "\n");

#ifdef DEBUG_SEPARATION
	if ( num_group > 1 )
		fprintf(stderr, "Analyzed Group %d: %d points, %d groups are found (%d tried)\n", groupId, numPoint, num_group, trial); 
#endif

	return num_group;

}

int	Group::seperate(GroupDump *dump)
{
	int i, j, max_tag, num_group;
	unsigned int mask;
	int			*num_points;
	int			missing_index;
	GroupEntity **first_entities;
	GroupEntity **last_entities;
	double		*pos_sumX, *pos_sumY, *pos_sumZ;
	double		*pos_ssumX, *pos_ssumY, *pos_ssumZ;

	// Prepare data structure
	max_tag = dump->max_tag;
	first_entities	= (GroupEntity **)malloc(sizeof(GroupEntity *) * (max_tag+2));
	last_entities	= (GroupEntity **)malloc(sizeof(GroupEntity *) * (max_tag+2));
	pos_sumX	= (double *)malloc(sizeof(double) * (max_tag+2));
	pos_sumY	= (double *)malloc(sizeof(double) * (max_tag+2));
	pos_sumZ	= (double *)malloc(sizeof(double) * (max_tag+2));
	pos_ssumX	= (double *)malloc(sizeof(double) * (max_tag+2));
	pos_ssumY	= (double *)malloc(sizeof(double) * (max_tag+2));
	pos_ssumZ	= (double *)malloc(sizeof(double) * (max_tag+2));
	num_points	= (int *)malloc(sizeof(int) * (max_tag+2));

	for ( i = 0 ; i <= max_tag + 1 ; i++ )
	{
		if ( dump->tag_mask[i>>5] & (1<<(i&0x1F)) )
		{
			first_entities[i] = new GroupEntity();
			last_entities[i] = new GroupEntity();
			first_entities[i]->pNext = last_entities[i];
			last_entities[i]->pPrev = first_entities[i];
		}
		else
		{
			first_entities[i] = NULL;
			last_entities[i] = NULL;
		}
		pos_sumX[i]		= 0.;
		pos_sumY[i]		= 0.;
		pos_sumZ[i]		= 0.;
		pos_ssumX[i]	= 0.;
		pos_ssumY[i]	= 0.;
		pos_ssumZ[i]	= 0.;
		num_points[i]	= 0;
	}

	missing_index = max_tag + 1;
	first_entities[missing_index] = new GroupEntity();
	last_entities[missing_index] = new GroupEntity();
	first_entities[missing_index]->pNext	= last_entities[missing_index];
	last_entities[missing_index]->pPrev	= first_entities[missing_index];

	// Seperate group entities into new groups
	GroupEntity *it		= firstEntity->pNext;
	firstEntity->pNext	= lastEntity;
	lastEntity->pPrev	= firstEntity;

	int index = 0;
	int missing1 = 0;
	int missing2 = 0;
	while ( it != lastEntity )
	{
		GroupEntity *entity;
		PointEntry *point = it->pPointEntry;
		assert(point);

		for ( i = index; i <  dump->numPoint ; i++ )
		{
			if ( it->id == dump->list[i].id )
				break;
			missing2++;
		}

		entity = it;
		it = it->pNext;

		int tag;

		if ( i < dump->numPoint )
		{

			tag = dump->list[i].tag;
			index = i+1;
		}
		else
		{
			tag = missing_index;
			missing1++;
		}

//		assert(first_entities[tag]);
		if (!first_entities[tag])
			*(int *)0 = 0;
		assert(last_entities[tag]);

		GroupEntity *first, *last;

		first	= first_entities[tag];
		last 	= last_entities[tag];

		entity->pPrev		= last->pPrev;
		entity->pNext 		= last;

		last->pPrev->pNext	= entity;
		last->pPrev			= entity;

		pos_sumX[tag]		+= dump->list[i].x;
		pos_sumY[tag]		+= dump->list[i].y;
		pos_sumZ[tag]		+= dump->list[i].z;
		pos_ssumX[tag]		+= dump->list[i].x * dump->list[i].x;
		pos_ssumY[tag]		+= dump->list[i].y * dump->list[i].y;
		pos_ssumZ[tag]		+= dump->list[i].z * dump->list[i].z;
		num_points[tag]++;
	}

#ifdef DEBUG_SEPARATION
	fprintf(stderr, "Group seperation finalized... (%d %d missed)\n", missing1, missing2);
#endif

	// Finding a group with most points
	int max_group = 0;;
	int max_points = 0;
	for ( i = 0 ; i <= max_tag ; i++ )
	{
#ifdef DEBUG_SEPARATION
		if ( first_entities[i] )
		{
			fprintf(stderr, "id:%2d %d points (%f,%f,%f) [%f %f %f]\n", i, num_points[i],
					pos_sumX[i]/num_points[i],
					pos_sumY[i]/num_points[i],
					pos_sumZ[i]/num_points[i],
					pos_ssumX[i]/num_points[i] - pos_sumX[i]*pos_sumX[i]/num_points[i]/num_points[i],
					pos_ssumY[i]/num_points[i] - pos_sumY[i]*pos_sumY[i]/num_points[i]/num_points[i],
					pos_ssumZ[i]/num_points[i] - pos_sumZ[i]*pos_sumZ[i]/num_points[i]/num_points[i]);
		}
#endif

		if ( num_points[i] > max_points )
		{
			max_points	= num_points[i];
			max_group	= i;
		}
	}
#ifdef DEBUG_SEPARATION
	fprintf(stderr, "id:xx %d points\n", num_points[missing_index]);
	fprintf(stderr, "Max_Group: %d\n", max_group);
	fprintf(stderr, "\n");
#endif

	if ( max_points == 0 )
	{
		max_group = missing_index;
	}

	// Attach the group with the most points to the previous (this) group
	delete firstEntity;
	delete lastEntity;
	
	firstEntity	= first_entities[max_group];
	lastEntity	= last_entities[max_group];
	firstEntity->pNext->pPrev = firstEntity;
	lastEntity->pPrev->pNext = lastEntity;
	pos_sum[0]			= pos_sumX[max_group];
	pos_sum[1]			= pos_sumY[max_group];
	pos_sum[2]			= pos_sumZ[max_group];
	pos_ssum[0]			= pos_ssumX[max_group];
	pos_ssum[1]			= pos_ssumY[max_group];
	pos_ssum[2]			= pos_ssumZ[max_group];
	numPoint 			= num_points[max_group];

	// attach missing entities to the group with the most points
	if ( num_points[missing_index] > 0 )
	{
		first_entities[missing_index]->pNext->pPrev = lastEntity->pPrev;
		last_entities[missing_index]->pPrev->pNext = lastEntity;
		lastEntity->pPrev->pNext	= first_entities[missing_index]->pNext;
		lastEntity->pPrev			= last_entities[missing_index]->pPrev;
		pos_sum[0]			+= pos_sumX[missing_index];
		pos_sum[1]			+= pos_sumY[missing_index];
		pos_sum[2]			+= pos_sumZ[missing_index];
		pos_ssum[0]			+= pos_ssumX[missing_index];
		pos_ssum[1]			+= pos_ssumY[missing_index];
		pos_ssum[2]			+= pos_ssumZ[missing_index];
		numPoint 			+= num_points[missing_index];
	}
	
	setDirty();
	check();

	delete first_entities[missing_index];
	delete last_entities[missing_index];

	first_entities[max_group]	= NULL;
	last_entities[max_group]	= NULL;
	num_points[max_group]		= 0;

	// Generate groups for other points
	for ( i = 0 ; i <= max_tag ; i++ )
	{
		if ( num_points[i] == 0 )
			continue;

		Group *grp = Group::create();

		delete grp->firstEntity;
		delete grp->lastEntity;

		grp->firstEntity				= first_entities[i];
		grp->lastEntity					= last_entities[i];
		grp->firstEntity->pNext->pPrev	= grp->firstEntity;
		grp->lastEntity->pPrev->pNext	= grp->lastEntity;

		GroupEntity *entity = grp->firstEntity->pNext;
		while ( entity != grp->lastEntity )
		{
			entity->pPointEntry->grp = grp;
			entity = entity->pNext;
		}
		grp->pos_sum[0]		= pos_sumX[i];
		grp->pos_sum[1]		= pos_sumY[i];
		grp->pos_sum[2]		= pos_sumZ[i];
		grp->pos_ssum[0]	= pos_ssumX[i];
		grp->pos_ssum[1]	= pos_ssumY[i];
		grp->pos_ssum[2]	= pos_ssumZ[i];
		grp->numPoint 		= num_points[i];
		grp->setDirty();

#ifdef DEBUG_SEPARATION
		fprintf(stderr, "Check tag %d...\n", i);
#endif
		grp->check();
	}

end:
	free(first_entities);
	free(last_entities);
	free(pos_sumX);
	free(pos_sumY);
	free(pos_sumZ);
	free(pos_ssumX);
	free(pos_ssumY);
	free(pos_ssumZ);
	free(num_points);

}

void Group::getPos(double *pos_)
{
	if ( dirty )
		stat();

	pos_[0] = pos[0];
	pos_[1] = pos[1];
	pos_[2] = pos[2];
}

void Group::getVar(double *var_)
{
	if ( dirty )
		stat();

	var_[0] = var[0];
	var_[1] = var[1];
	var_[2] = var[2];
}

void Group::setDirty(void)
{
	dirty = true;
}


double getRandomNormal(void)
{
#if 0
	double z0, z1;
	double u1, u2;
	double two_pi = 2*M_PI;
	do
	{
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	}
	while ( u1 <= 0.0001 );
		
	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);

	return z0;
#else

	double x1, x2, w, y1, y2;
	 
	do {
	x1 = 2.0 * rand() / RAND_MAX - 1.0;
	x2 = 2.0 * rand() / RAND_MAX - 1.0;
	w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );
	
	w = sqrt( (-2.0 * log( w ) ) / w );
	y1 = x1 * w;
	y2 = x2 * w;

	return y1;
#endif
}

void Group::generateParticles(Slice *slice)
{
	int i,j;
	double _pos[3];
	double _var[3];
	double std[3];

	double dp = (pos_p[0]*pos_p[0] + pos_p[1]*pos_p[1] + pos_p[2]*pos_p[2]);

	// Reset particle filter estimation
//	dp = 0;
	if ( dp == 0. )
	{
		getPos(_pos);
		_var[0] = var[0]*4.;
		_var[1] = var[1]*4.;
		_var[2] = var[2]*4.;
	}
	else
	{
		_pos[0] = pos_p[0];
		_pos[1] = pos_p[1];
		_pos[2] = pos_p[2];
		_var[0] = var_p[0];
		_var[1] = var_p[1];
		_var[2] = var_p[2];
	}
	_var[0] = var[0]*4.;
	_var[1] = var[1]*4.;
	_var[2] = var[2]*4.;

	std[0] = sqrt(_var[0]);
	std[1] = sqrt(_var[1]);
	std[2] = sqrt(_var[2]);

	int numLaser = 0;
	PointEntry *points = (PointEntry *)malloc(sizeof(PointEntry) * SLICE_SIZE);
	bool *shot = (bool *)malloc(sizeof(bool) * SLICE_SIZE);
//	fprintf(stderr, "================================\nLaser pointer\n");
	for ( i = 0 ; i < SLICE_SIZE ; i++ )
	{
		PointEntry *point;
		if ( slice->entity[i] == NULL )
			continue;
		point = slice->entity[i]->point;
		if ( point->grp == this )
			shot[numLaser] = true;
		else
			shot[numLaser] = false;
		points[numLaser].x = point->x;
		points[numLaser].y = point->y;
		points[numLaser].z = point->z;
		numLaser++;
//		fprintf(stderr, "%d: %f %f %f\n", i, point->x, point->y, point->z);
	}
	
	double offset[3] = {0.};
	for ( i = 0 ; i < NUM_PARTICLE ; i++ )
	{
		px[i] = _pos[0] + getRandomNormal() * std[0];
		py[i] = _pos[1] + getRandomNormal() * std[1];
		pz[i] = _pos[2] + getRandomNormal() * std[2];
		pp[i] = 1. / NUM_PARTICLE;

		offset[0] += _pos[0] - px[i];
		offset[1] += _pos[1] - py[i];
		offset[2] += _pos[2] - pz[i];
	}
	offset[0] = offset[0] / NUM_PARTICLE;
	offset[1] = offset[1] / NUM_PARTICLE;
	offset[2] = offset[2] / NUM_PARTICLE;

	double psum = 0.;
	for ( i = 0 ; i < NUM_PARTICLE ; i++ )
	{
		double min_dist1 = 10000. , dist1;
		double min_dist2 = 10000. , dist2;

		px[i] += offset[0];
		py[i] += offset[1];
		pz[i] += offset[2];
		pp[i] = 1.;

		for ( j = 0 ; j < numLaser ; j++ )
		{

			if ( shot[j] )
			{
#if 1
				dist1 =	(px[i] - points[j].x)*(px[i] - points[j].x)/(2.*_var[0]) +
						(py[i] - points[j].y)*(py[i] - points[j].y)/(2.*_var[1]) +
						(pz[i] - points[j].z)*(pz[i] - points[j].z)/(2.*_var[2]);
#else
				dist2 =	(px[i] - points[j].x)*(px[i] - points[j].x)/.1 +
						(py[i] - points[j].y)*(py[i] - points[j].y)/.1 +
						(pz[i] - points[j].z)*(pz[i] - points[j].z)/.1;
#endif

				if ( dist1 < min_dist1 )
				{
					min_dist1 = dist1;
				}
			}

			double t1, t2, t3;
			t1 =	(py[i]*points[j].z - pz[i]*points[j].y);
			t2 =	(pz[i]*points[j].x - px[i]*points[j].z);
			t3 =	(px[i]*points[j].y - py[i]*points[j].x);

			dist2 =	t1*t1 + t2*t2 + t3*t3;
			
			if ( dist2 < min_dist2 )
			{
				min_dist2 = dist2;
			}
		}
		
		if ( min_dist1 < 1.0 )
			pp[i] *= 1.5; //exp(-1.01*min_dist2);// * pp[i];
//		else if ( min_dist2 < 1.0 )
//			pp[i] *= 0.5;

		psum += pp[i];
	}

	double sum[3] = {0};
	double ssum[3] = {0};
	for ( i = 0 ; i < NUM_PARTICLE ; i++ )
	{
		sum[0] += px[i] * pp[i];
		sum[1] += py[i] * pp[i];
		sum[2] += pz[i] * pp[i];
		ssum[0] += px[i] * px[i] * pp[i];
		ssum[1] += py[i] * py[i] * pp[i];
		ssum[2] += pz[i] * pz[i] * pp[i];
	}

	pos_p[0] = sum[0] / psum;
	pos_p[1] = sum[1] / psum;
	pos_p[2] = sum[2] / psum;
	var_p[0] = ssum[0] / psum - pos_p[0] * pos_p[0];
	var_p[1] = ssum[1] / psum - pos_p[1] * pos_p[1];
	var_p[2] = ssum[2] / psum - pos_p[2] * pos_p[2];

	fprintf(stderr, "Particle P %f %f %f (%f)\n", pos_p[0], pos_p[1], pos_p[2], psum);
	free(points);
}

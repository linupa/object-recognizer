#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <assert.h>

#include "Octree.h"
#include "DataStructure.h"

#ifdef SLICE_ENTITY_BUFFER
SliceEntity *SEList;
int se_index = 0;
#endif

Slice* Slice::pBaseSlice = NULL;
Slice* Slice::pLastSlice = NULL;
int Slice::count = 0;


void Slice::initialize(void)
{
#ifdef SLICE_ENTITY_BUFFER
	SEList = (SliceEntity *)malloc(sizeof(SliceEntity) * 100000);
	se_index = 0;
#endif
}

Slice::Slice(void)
{
	memset(entity, 0, sizeof(SliceEntity *) * SLICE_SIZE);
	pNext = NULL;
	count++;
}

void Slice::addPoint(class PointEntry *pPoint, int index)
{

#ifdef SLICE_ENTITY_BUFFER
    SliceEntity *newEntity = &(SEList[se_index++]);
    newEntity->slice   = NULL;
    newEntity->index   = 0;
	newEntity->point	= NULL;
    newEntity->pNext   = NULL;
	newEntity->pPrev	= NULL;
#else
    SliceEntity *newEntity = new SliceEntity;   
#endif
	newEntity->slice 	= this;
	newEntity->index	= index;
	newEntity->point	= pPoint;

	entity[index ] = newEntity;
    
#if 0
    SliceEntity *last = pPoint->slices;
    while ( last->pNext ) 
        last = last->pNext;
    last->pNext = newEntity;

	newEntity->pPrev	= last;
	newEntity->pNext	= NULL;
#else
	newEntity->pNext	= pPoint->slices->pNext;
	newEntity->pPrev	= pPoint->slices;
	pPoint->slices->pNext	= newEntity;
	if ( newEntity->pNext )
		newEntity->pNext->pPrev = newEntity;
#endif
}

void Slice::reset(void)
{
	while ( pBaseSlice )
	{
		Slice  *delSlice;

		delSlice = pBaseSlice;
		pBaseSlice = pBaseSlice->pNext;
	
		delete delSlice;
	}
	pBaseSlice = new Slice;
	pLastSlice = pBaseSlice;
	pBaseSlice->id = -1;
}

void Slice::deleteBefore(int id)
{
	int count = 0;

	if ( id < 0 )
		return;
	Slice *entity = pBaseSlice;
	while ( entity->pNext ) 
	{
		
		int entity_id = entity->pNext->id;
		if ( entity_id >= 0 && entity_id < id )
		{
			Slice *delSlice = entity->pNext;

			entity->pNext = delSlice->pNext;

			delete delSlice;
			count++;
		}
		else 
			entity  = entity->pNext;
	}
	pLastSlice = entity;
}

SliceEntity::SliceEntity(void)
{
    slice   = NULL;
    index   = 0;
	point	= NULL;
    pNext   = NULL;
	pPrev	= NULL;
}

SliceEntity::~SliceEntity(void)
{
    if ( slice )
        slice->entity[index] = NULL;

	if ( pPrev )
		pPrev->pNext = pNext;
	if ( pNext )
		pNext->pPrev = pPrev;

	// Delete PointEntity
	if ( point && point->slices->pNext == NULL )
		delete point;
}

Slice::~Slice(void)
{
	for ( int i = 0 ; i < SLICE_SIZE ; i++ )
	{
		if ( entity[i] )
#ifdef SLICE_ENTITY_BUFFER
			entity[i]->~SliceEntity();
#else
			delete entity[i];
#endif

	}
	count--;
}

Slice * Slice::create(int id_)
{
	Slice *slice = new Slice();	

	slice->id = id_;
	
	assert(pBaseSlice);
#if 0
	Slice *last = pBaseSlice;
	while ( last->pNext != NULL )
		last = last->pNext;
	last->pNext = slice;
#else
	pLastSlice->pNext = slice;
	pLastSlice = slice;
#endif

	return slice;
}

void Slice::log(void)
{
	fprintf(stderr, "Slice Count: %d\n", count);
}

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <sys/time.h>
#include <unistd.h>

#include <Eigen/Dense>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Slider.H>

#include "fileio.h"
#include "Octree.h"
#include "Comm.h"
#include "DataStructure.h"
#include "MySim.h"



#undef DEBUG

using namespace std;


double scale;

double	_floor = DEFAULT_FLOOR;
double	ceiling = DEFAULT_CEILING;
int		group_threshold = 50;

double highlight = 0.0;

static int win_width(800);
static int win_height(600);
static char const * win_title("Object Recognition");
static bool paused = false;
static bool file_stream = true;

Comm *pRecvComm = NULL;
Comm *pSendComm = NULL;

int slice_idx = 0;
int window_size = 60;
using namespace std;
using Eigen::MatrixXf;
using Eigen::MatrixXd;
using Eigen::VectorXd;

double 		dt = 0.03;
bool 		gSeperate = true;
int 		gMode = 0;
bool 		gTrack = false;
bool 		gShowBase = true;
int 		trackingGroupIndex = 0;
int 		trackingGroupId = 0;
double 		trackPos[3];
HumanModel 	*human = NULL;
HumanModel 	*human_result = NULL;
HumanModel	*human_request = NULL;
bool		human_detection_idle = true;
bool		regenHuman = true;

bool crash_debug = false;

Group *track(void);

pthread_mutex_t	mutex;
void periodic_task(MySim *sim);
void *receive(void*);
void *detectHumanBody(void *);

bool seperation_consistency = false;

// Checking process time
int start_time_sec;
int start_time_usec;
int ts[10][2] = {{0}};
void reset_timer(void)
{
	for ( int i = 0 ; i < 10 ; i++ )
		ts[i][0] = ts[i][1] = 0;
}
void mark_start_time(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	start_time_sec	= tv.tv_sec;
	start_time_usec	= tv.tv_usec;
}

int get_elapsed(void)
{
	struct timeval tv;

	int usec;
	int sec;

	gettimeofday(&tv, NULL);

	usec	= tv.tv_usec - start_time_usec;
	sec		= tv.tv_sec - start_time_sec;

	return (sec*1000000 + usec); 
}


int num_slice;

PointEntry *highlighted = NULL;
slice_t **ppSlices = NULL;
Octree *base_octree;
#if 0
SliceEntry::SliceEntry(class PointEntry *point)
{
	pPrev = pNext = NULL;
	pPointEntry = point;
}
#endif


#define SET_SIZE	(50)
#define NUM_SET		(1081/SET_SIZE)
double a[NUM_SET];
double b[NUM_SET];
//double a = (double *)malloc(sizeof(double)*NUM_SET);
//double b = (double *)malloc(sizeof(double)*NUM_SET);
void preprocessSlice(slice *slice_)
{
	double phi, theta, depth;
	double x, y, z;
	int i;

	if ( slice_ == NULL )
		return;

	slice_->valid = true;

	double prev[3] = {0.,0.,0.};
	int seg_idx = 0;
	double last_depth = -10.;
	double			xx, yy, sx, sy, xy;
	int				n;
	double			sth, ssth;
	double			sdth, ssdth;
	double			prev_th = slice_->theta[0];

	sth = ssth = 0.;
	sdth = ssdth = 0.;
	xx = yy = sx = sy = xy = 0.;
	n = 0;
	for ( i = 0 ; i < slice_->num ; i++ )
	{
		double	dx[3];
		double	dl;
		double	dth;

		x		= slice_->x[i];
		y		= slice_->y[i];
		z		= slice_->z[i];
		phi		= slice_->phi[i];
		theta	= slice_->theta[i];
		depth	= slice_->depth[i];
		dth		= fabs(theta - prev_th);

		if ( dth > M_PI )
			dth -= 2.*M_PI;
		else if ( dth < -M_PI )
			dth += 2.*M_PI;

		sth		+= theta;
		ssth	+= theta*theta;

		sdth	+= dth;
		ssdth	+= dth*dth;

		dx[0] = x - prev[0];
		dx[1] = y - prev[1];
		dx[2] = z - prev[2];
		prev[0] = x;
		prev[1] = y;
		prev[2] = z;
		dl = sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]);

		slice_->dx[i]	= dx[0];
		slice_->dy[i]	= dx[1];
		slice_->dz[i]	= dx[2];
		if ( dl > 0. )
			slice_->corr[i] = 1 - (x*dx[0] + y*dx[1] + z*dx[2]) / (depth * dl);
		else
			slice_->corr[i] = 0.5;
		slice_->weight[i] = slice_->corr[i];
		
		double dist = 0.;
		double a_ = slice_->a[seg_idx];
		double b_ = slice_->b[seg_idx];

		x = - depth * sin(phi);
		y = - depth * cos(phi);
		if ( a_ != 0. || b_ != 0. )
		{
			dist = (a_*x + b_*y - 1.)*(a_*x + b_*y - 1.) / (a_*a_ + b_*b_);
		}
		double reg_th = depth*0.01;
		double con_th = max(0.05, depth*MERGE_THRESHOLD);
		if ( fabs(depth - last_depth) > con_th || (slice_->size[seg_idx] > 5 && dist > reg_th*reg_th) )
		{
			seg_idx++;	
			xx = yy = sx = sy = xy = 0.;
			n = 0;
			slice_->a[seg_idx] = 0.;
			slice_->b[seg_idx] = 0.;
			slice_->size[seg_idx] = 0;
		}
		slice_->segment[i] = seg_idx;

		xx	+= x*x;
		yy	+= y*y;
		xy	+= x*y;
		sx	+= x;
		sy	+= y;
		n	+= 1;
		double det = xx*yy - xy*xy;
		if ( fabs(det) > 0.001 )
		{
			slice_->a[seg_idx] = 1. / det * ( yy * sx - sy * xy );
			slice_->b[seg_idx] = 1. / det * ( -xy * sx + xx * sy );
			slice_->distance[seg_idx] = (xx+yy) / (double)n;
		}
		else
		{
			slice_->a[seg_idx] = slice_->b[seg_idx] = 0.;
		}
		slice_->size[seg_idx]++;


		if ( dth > M_PI/2. && dth < 3*M_PI/2. )
			slice_->valid = false;


		last_depth = depth;
		prev_th = theta;
	}

	double pass_threshold = .2;
	for ( i = 0 ; i < slice_->num ; i++ )
	{
		int seg_idx = slice_->segment[i];
		double a, b, d;

		a = slice_->a[seg_idx];
		b = slice_->b[seg_idx];
		d = slice_->distance[seg_idx];

		double threshold = 2./pass_threshold/pass_threshold;
		
		if ( slice_->size[seg_idx] < 10 || (a*a + b*b) > threshold || (a == 0. && b == 0.) )
			slice_->segment[i] = 0;
	}

	double var_th	= fabs(sth*sth - ssth*slice_->num);
	double var_dth	= fabs(sdth*sdth - ssdth*slice_->num); 

//	if ( var_th < 10. )
//		slice_->valid = false;

	if ( var_dth > 1.*1. )
		slice_->valid = false;

//	fprintf(stderr, "VAR: %f %f\n", var_th, var_dth);

}

void generatePoints(slice *slice_, bool verbose)
{
	int i;
	Group *prevGroup = NULL;

	if ( slice_ == NULL )
		return;

	if ( !slice_->valid )
		return;

//	int added = 0;
	int prevSeg = -1;

	Slice *newSlice = Slice::create(slice_->id);

//	fprintf(stderr, "Slice %d\n", slice_->id);
	bool checked = false;
	for ( i = 0 ; i < slice_->num ; i++ )
	{
		OctreeEntry 	*ret;
		double 			x, y, z, depth;
		int 			seg;
		Slice 			*it;
		PointEntry 		*entry, *ret_entry;
        bool 			oldEntry;
		int 			ts0;
		OctreeEntryList	*online;
		bool			findNeighbor = true;

		mark_start_time();

		x = slice_->x[i];
		y = slice_->y[i];
		z = slice_->z[i];
		depth = slice_->depth[i];
		seg = slice_->segment[i];

		if ( z > ceiling )
			goto pass;
		if ( z < _floor )
			goto pass;

		if ( seg == 0 )
			goto pass;

		entry = new PointEntry();

		entry->x = x;
		entry->y = y;
		entry->z = z;
		entry->timestamp = slice_->timestamp;

		ret = base_octree->addEntry(entry, MAX_DEPTH);

		ts0 = get_elapsed();

		// Mostly, the point is out of octree area
		if ( ret == NULL )
		{
			assert(!entry->grp);
			assert(!entry->pGroupEntity);
			assert(!entry->tree); 
			assert(entry->slices); 
			delete entry;
			goto pass;
		}

        ret_entry = (PointEntry *)ret;
        
		// Net Octree Entry
        if ( ret_entry == entry )
		{
            oldEntry = false;
		}
		// Existing Octree Entry
        else
		{
			oldEntry = true;
            entry = ret_entry;

			// The entry is in new slice
			if ( entry->timestamp == slice_->timestamp )
				findNeighbor = false;
			// The entry is in old slice
			else
				entry->timestamp = slice_->timestamp;
		}

		ts[0][0] += ts0;
		ts[0][1]++;

		ts[1][0] += get_elapsed();
		ts[1][1]++;
		newSlice->addPoint(entry, i);

		ts[2][0] += get_elapsed();
		ts[2][1]++;

		// Octree is already occupied
        if ( oldEntry )
        {
//			Do nothing. Previous Group will be merged later
//            if ( prevSeg == seg )
//                assert(entry->grp == prevGroup);
//				Group::merge(prevGroup, entry->grp);
        }
		// The point is in the base
		else if ( Group::isBase(entry) )
		{
			Group::reserved[0]->addPoint(entry);
			findNeighbor = false;
			prevSeg = -1;
		}
		// The point is with the previous point
		else if ( prevSeg == seg && !prevGroup->isBase() )
		{
			prevGroup->addPoint(entry);
		}
		// The point creates a new group
		else
		{
			Group *grp = Group::create();
			grp->addPoint(entry);
		}
#ifdef DEBUG
		entry->grp->check();
#endif
//		std::cerr << "Add new Entry" << std::endl;

		ts[3][0] += get_elapsed();
		ts[3][1]++;

		if ( findNeighbor )
		{
			Group *group = entry->grp;
//			group->check();
			OctreeEntryList::reset();	
			double distance = sqrt(entry->x*entry->x+entry->y*entry->y+entry->z*entry->z);
			base_octree->getNeighbor(entry, MERGE_THRESHOLD );	
//			base_octree->getNeighbor(entry, MERGE_THRESHOLD * distance);	
			ts[4][0] += get_elapsed();
			ts[4][1]++;
			OctreeEntryList *neighbor = OctreeEntryList::list;
			int count = OctreeEntryList::count;
#ifdef SURFACE_NORMAL
			double *x = (double *)malloc(sizeof(double) * count );
			double *y = (double *)malloc(sizeof(double) * count );
			double *z = (double *)malloc(sizeof(double) * count );
#endif
			
			int idx = 0;
			int i, j;
			double normal[3];

			entry->contact = false;
			for ( i = 0 ; i < OctreeEntryList::count ; i++, neighbor++ )
			{
				PointEntry *n_entry = (PointEntry *)neighbor->pEntry;

				// Merge nearby groups
				if ( n_entry )
				{
					Group *n_group = n_entry->grp;

					// Measure the distance
					double xx, yy, zz, dist;

					xx = n_entry->x - entry->x;
					yy = n_entry->y - entry->y;
					zz = n_entry->z - entry->z;
					
					dist = xx*xx + yy*yy + zz*zz;

//					n_group->check();
					if ( group != n_group && !(group->isBase() ^ n_group->isBase()) )
					{
						group = Group::merge(group,n_group);	
#ifdef DEBUG
//						group->check();
#endif
					}

					// Check the point is close to the base
					if ( Group::reserved[0]->numPoint > 0 && !(group->isBase()) && n_group->isBase() && dist < (0.05*0.05) && entry->z > -.15)
						entry->contact = true;



#ifdef SURFACE_NORMAL
					// Preparing vecors for Surface normal
					if ( dist < MERGE_THRESHOLD*MERGE_THRESHOLD/4. )
						x[idx] = y[idx] = z[idx] = 0.;
					else
						x[idx] = xx, y[idx] = yy, z[idx] = zz;
#endif

				}
#ifdef SURFACE_NORMAL
				else
				{
					x[idx] = y[idx] = z[idx] = 0.;
				}
#endif

				idx++;
			}
			
#ifdef SURFACE_NORMAL
			// Generate Surface Normal
			normal[0] = normal[1] = normal[2] = 0.;
			for ( i = 0 ; i < count ;  i++ )
			{
				for ( j = i+1 ; j < count ; j++ )
				{
					double nx, ny, nz;
					double ip;

					nx += y[i] * z[j] - z[i] * y[j];
					ny += z[i] * x[j] - x[i] * z[j];
					nz += x[i] * y[j] - y[i] * x[j];

					ip = entry->x * nx + entry->y * ny + entry->z * nz;
					
					if ( ip > 0 )
					{
						nx*=-1., ny*=-1., nz*=-1.;
					}
					normal[0] += nx;
					normal[1] += ny;
					normal[2] += nz;
				}
			}
			double mag = sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2]);
			entry->nx = normal[0] / mag, entry->ny = normal[1] / mag, entry->nz = normal[2] / mag;

			free(x), free(y), free(z);
#endif
		}
		ts[5][0] += get_elapsed();
		ts[5][1]++;
	
//		if ( ret == false && fabs(xOctreeEntryList) < 2 && fabs(y) < 2 && fabs(z) < 2 )
//		{
//			fprintf(stderr, "Error to add entry (%f,%f,%f)\n", entry->x, entry->y, entry->z);
//		}

		// Removing points on the laser ray
		// ing...
#if 0
		double coeff[3];
		double absval;

		absval = sqrt(x*x + y*y + z*z);
		coeff[0] = x / absval;
		coeff[1] = y / absval;
		coeff[2] = z / absval;

		OctreeEntryList::reset();	

		base_octree->getLineDistance(coeff, 0.05);

		online = OctreeEntryList::list;
		while ( online )
		{
			if ( online->pEntry ) 
			{
				PointEntry *n_entry = (PointEntry *)online->pEntry;

				if ( n_entry->timestamp != slice_->timestamp )
				{
//					delete n_entry;
				}
			}

			online = online->pNext;
		}
#endif
		

//		if ( ret )
//			added++;


		prevGroup = entry->grp;
		prevSeg = seg;
		continue;

pass:
		prevGroup = NULL;
		prevSeg = -1;

	}
#if 1
		if ( base_octree->checkValidity() == false )
		{
			fprintf(stderr, "ERROR: %d\n", i);
		}
#endif
//	fprintf(stderr, "%d points added\n", added);
}




 
int drawn;
void MySim::
timer_cb(void * param)
{
	static double lastZ = 10.0;

	double x, z;
//	x = myModel.getX();
//	z = myModel.getZ();

//	if ( z <= 0.0 && lastZ <= 0.0 )
//		paused = true;
	
//	if ( !paused )
	{
//		myModel.update();

		lastZ = z; 

//		traceX.push_back(x);
//		traceZ.push_back(z);

		reinterpret_cast<MySim*>(param)->redraw();
	}
/*
	if ( ! paused || ! paused_ready ) {
		reinterpret_cast<Simulator*>(param)->tick();
	}

	if ( paused )
		paused_ready = true;
	if ( ! paused )
		paused_ready = false;
*/
	Fl::repeat_timeout(dt, // gets initialized within tick()
			   timer_cb,
			   param);

	periodic_task((MySim *)param);

}


class MyWindow : public Fl_Double_Window 
{
public:
	MyWindow(int width, int height, const char *title);

	virtual void resize(int x, int y, int w, int h);	

	Fl_Button *trackButton;
	Fl_Button *seperateButton;
	Fl_Button *modeButton;
	Fl_Button *baseButton;
	Fl_Button *learnBaseButton;
	Fl_Button *iterateButton;
	Fl_Button *quitButton;
	MySim *sim;
	MyStat *stat;
	Fl_Value_Slider *mGroupSlider;
	Fl_Value_Slider *mSizeSlider;
	Fl_Value_Slider *mSliceSlider;
	Fl_Value_Slider *mScaleSlider;
	Fl_Value_Slider *mFloorSlider;
	Fl_Value_Slider *mCeilingSlider;
	Fl_Value_Slider *mHighlightSlider;

	static void cb_group(Fl_Widget *widget, void *param);
	static void cb_size(Fl_Widget *widget, void *param);
	static void cb_floor(Fl_Widget *widget, void *param);
	static void cb_ceiling(Fl_Widget *widget, void *param);

	static void cb_track(Fl_Widget *widget, void *param);
	static void cb_showBase(Fl_Widget *widget, void *param);
	static void cb_learnBase(Fl_Widget *widget, void *param);
	static void cb_seperate(Fl_Widget *widget, void *param);
	static void cb_iterate(Fl_Widget *widget, void *param);

	static void cb_highlight(Fl_Widget *widget, void *param);

	static void cb_slice(Fl_Widget *widget, void *param);
	static void cb_scale(Fl_Widget *widget, void *param);
	static void cb_quit(Fl_Widget *widget, void *param);

	static void cb_mode(Fl_Widget *widget, void *param);
};

MyWindow:: 
MyWindow(int width, int height, const char * title) 
    : Fl_Double_Window(width, height, title) 
{ 
	fprintf(stderr, "Window created\n");
    Fl::visual(FL_DOUBLE|FL_INDEX); 
    begin(); 
    stat = new MyStat(0, 0, width, height - 40); 
    sim = new MySim(0, 0, width, height - 40); 
//	stat->hide();
//	sim->show();

    trackButton = new Fl_Button(5, height - 35, 100, 30, "&TrackOff"); 
    trackButton->callback(cb_track, this); 
    seperateButton = new Fl_Button(5, height - 35, 100, 30, "&SepOn"); 
    seperateButton->callback(cb_seperate, this); 
    modeButton = new Fl_Button(5, height - 35, 100, 30, "&Mode0"); 
    modeButton->callback(cb_mode, this); 
    baseButton = new Fl_Button(5, height - 35, 100, 30, "&BaseShow"); 
    baseButton->callback(cb_showBase, this); 
    learnBaseButton = new Fl_Button(5, height - 35, 100, 30, "&Learn"); 
    learnBaseButton->callback(cb_learnBase, this); 
    iterateButton = new Fl_Button(5, height - 35, 100, 30, "&Iterate"); 
    iterateButton->callback(cb_iterate, this); 

	printf("WIN: %p\n", this);
    quitButton = new Fl_Button(width - 105, height - 35, 100, 30, "&Quit"); 
    quitButton->callback(cb_quit, this); 

	mGroupSlider = new Fl_Value_Slider( 10, height - 30, width / 4 - 100, 30, "");
	mGroupSlider->type(FL_HORIZONTAL);
	mGroupSlider->bounds(0., (double)1000.);
	mGroupSlider->callback(cb_group, this);
	mGroupSlider->step(1.);
	mGroupSlider->value(group_threshold);

	mSizeSlider = new Fl_Value_Slider( 10, height - 30, width / 4 - 100, 30, "");
	mSizeSlider->type(FL_HORIZONTAL);
	mSizeSlider->bounds(1., 400.);
	mSizeSlider->callback(cb_size, this);
	mSizeSlider->step(1.);
	mSizeSlider->value(window_size);

	mSliceSlider = new Fl_Value_Slider( 10, height - 30, width / 4 - 100, 30, "");
	mSliceSlider->type(FL_HORIZONTAL);
	mSliceSlider->bounds(0., (double)num_slice-1.);
	mSliceSlider->callback(cb_slice, this);
	mSliceSlider->step(1.);
	mSliceSlider->value(slice_idx);

	mScaleSlider = new Fl_Value_Slider( width / 4 + 20, height - 30, width / 4 - 100, 30, "");
	mScaleSlider->type(FL_HORIZONTAL);
	mScaleSlider->bounds(0.01, 100.);
	mScaleSlider->step(0.01);
	mScaleSlider->callback(cb_scale, this);
	mScaleSlider->value(scale);

	mFloorSlider = new Fl_Value_Slider( width / 4 + 20, height - 30, width / 4 - 100, 30, "");
	mFloorSlider->type(FL_HORIZONTAL);
	mFloorSlider->bounds(-3., 0.);
	mFloorSlider->step(0.1);
	mFloorSlider->callback(cb_floor, this);
	mFloorSlider->value(_floor);

	mCeilingSlider = new Fl_Value_Slider( width / 4 + 20, height - 30, width / 4 - 100, 30, "");
	mCeilingSlider->type(FL_HORIZONTAL);
	mCeilingSlider->bounds(0., 3.);
	mCeilingSlider->step(0.1);
	mCeilingSlider->callback(cb_ceiling, this);
	mCeilingSlider->value(ceiling);

	mHighlightSlider = new Fl_Value_Slider( 10, height - 35, width / 4 - 100, 30, "");
	mHighlightSlider->type(FL_VERTICAL);
	mHighlightSlider->bounds(0., (double)1080.-1.);
	mHighlightSlider->callback(cb_highlight, this);
	mHighlightSlider->step(1.);
	mHighlightSlider->value(highlight);

    end(); 
    resizable(this); 
	resize(0,0,width, height);
    show(); 
}

void MyWindow::
resize(int x, int y, int w, int h)
{

	Fl_Double_Window::resize(x, y, w, h);
	sim->resize(0, 0, w-70, h-110);
	stat->resize(0, 0, w-70, h-110);
	mSliceSlider->resize(10, h-35, (w-230)/2 - 5,25);
	mScaleSlider->resize(10 + (w-230)/2 - 5, h-35, (w-230)/2 - 5,25);
	mCeilingSlider->resize(10, h-70, (w-230)/2 - 5,25);
	mFloorSlider->resize(10 + (w-230)/2 - 5, h-70, (w-230)/2 - 5,25);
	mGroupSlider->resize(10, h-105, (w-230)/2 - 5,25);
	mSizeSlider->resize(10 + (w-230)/2 - 5, h-105, (w-230)/2 - 5,25);
	mHighlightSlider->resize(w-40, 10, 30, h-60);

	seperateButton->resize(	w-225,	h - 105, 70, 25);
	iterateButton->resize(	w-150,	h - 105, 70, 25);
	trackButton->resize(	w-225,	h - 70, 70, 25);
	baseButton->resize(		w-150,	h - 70, 70, 25);
	modeButton->resize(		w-225,	h - 35, 70, 25);
	learnBaseButton->resize(w-150,	h - 35, 70, 25);
	quitButton->resize(		w-75,	h - 35, 70, 25);

}
  
void MyWindow::
cb_track(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MySim *sim = reinterpret_cast<MyWindow*>(param)->sim;
	if ( gTrack )
	{
		gTrack = false;
		button->label("&TrackOff");
		sim->trj.clear();
	}
	else
	{
		gTrack = true;
		button->label("&TrackOn");
	}
}

void MyWindow::
cb_seperate(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MySim *sim = reinterpret_cast<MyWindow*>(param)->sim;
	if ( gSeperate )
	{
		gSeperate = false;
		button->label("&SepOff");
	}
	else
	{
		gSeperate = true;
		button->label("&SepOn");
	}
}

void MyWindow::
cb_iterate(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MySim *sim = reinterpret_cast<MyWindow*>(param)->sim;

	if ( human )
	{
		human->gmm->iterate();
		human->hand->iterate();
	}
}

void MyWindow::
cb_mode(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MyWindow *win = (MyWindow *)param;
	if ( gMode )
	{
		gMode = 0;
		button->label("&Mode0");
		win->stat->hide();
		win->sim->show();
	}
	else
	{
		gMode = 1;
		button->label("&Mode1");
		win->sim->hide();
		win->stat->show();
	}
}

void MyWindow::
cb_showBase(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MySim *sim = reinterpret_cast<MyWindow*>(param)->sim;
	if ( gShowBase )
	{
		gShowBase = false;
		button->label("&BaseHide");
	}
	else
	{
		gShowBase = true;
		button->label("&BaseShow");
	}
}
void MyWindow::
cb_learnBase(Fl_Widget *widget, void *param)
{
	Fl_Button *button = (Fl_Button *)widget;
	MySim *sim = reinterpret_cast<MyWindow*>(param)->sim;

	PointEntry entry;

	entry.x = 0.;
	entry.y = 0.;
	entry.z = -0.2;

	int count = 0;
	OctreeEntryList::reset();	
	base_octree->getNeighbor(&entry, .6 );	

#if 1
	OctreeEntryList *neighbor = OctreeEntryList::list;

	cout << "BEFORE " << Group::reserved[0]->numPoint << endl;
	for ( int i = 0 ; i < OctreeEntryList::count ; i++, neighbor++ )
	{
		bool inBase = true;
		PointEntry *n_entry = (PointEntry *)neighbor->pEntry;

		if ( n_entry->z > -0.1 )
			inBase = false;

		if ( inBase )
		{
			Group::removePoint(n_entry);
			Group::reserved[0]->addPoint(n_entry);
			assert(n_entry->tree);
			count++;
		}
	}
#endif

	cout << "ADDED " << count << endl;

	if ( Group::reserved[0]->numPoint > 0 )
	{
		GroupDump *dump = new GroupDump(Group::reserved[0]);

		cout << "SORT " << Group::reserved[0]->numPoint << endl;

		dump->sort();
		dump->seperate(MERGE_THRESHOLD);
		Group::reserved[0]->seperate(dump);
		
		Group::defineBase();
	}
}
void MyWindow::
cb_quit(Fl_Widget *widget, void *param)
{
	reinterpret_cast<MyWindow*>(param)->hide();
}

void MyWindow::
cb_group(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	group_threshold = (int)pSlider->value();

}

void MyWindow::
cb_size(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	int old_size = window_size;
	window_size = (int)pSlider->value();

	seperation_consistency = false;
	Group::reset();
	Slice::reset();
	base_octree->deleteChild();
	int first = slice_idx - window_size;
	if ( first < 0 )
		first = 0;
	reset_timer();
	for ( int i = first ; i <= slice_idx ; i++ )
	{
		preprocessSlice(ppSlices[i]);
		generatePoints(ppSlices[i], (i==slice_idx-1)?true:false);
	}
	highlighted = NULL;
	regenHuman = true;
	Slice::deleteBefore(first);
	base_octree->removeEmpty();
}

void MyWindow::
cb_slice(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	int old_index = slice_idx;
	slice_idx = (int)pSlider->value();

	int first = slice_idx - window_size;

	if ( ( first > old_index ) || ( old_index > slice_idx ) )
	{
		if ( first < 0 )
			first = 0;
		seperation_consistency = false;
		Group::reset();
		Slice::reset();
		base_octree->deleteChild();
		old_index = first;
		reset_timer();
	}

	for ( int i = old_index+1 ; i <= slice_idx ; i++ )
	{
#if 0
		generatePoints(ppSlices[i], (i==slice_idx-1)?true:false);
#else
		preprocessSlice(ppSlices[i]);
		generatePoints(ppSlices[i], (47<=slice_idx-1)?true:false);
#endif
	}
	highlighted = NULL;
	regenHuman = true;

	Slice::deleteBefore(first);
	base_octree->removeEmpty();
}

void MyWindow::
cb_scale(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	scale = pSlider->value();
}

void MyWindow::
cb_floor(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	_floor = pSlider->value();
 
	seperation_consistency = false;
	Group::reset();
	Slice::reset();
	base_octree->deleteChild();
	int first = slice_idx - window_size;
	if ( first < 0 )
		first = 0;
	reset_timer();
	for ( int i = first ; i <= slice_idx ; i++ )
	{
		preprocessSlice(ppSlices[i]);
		generatePoints(ppSlices[i], (i==slice_idx-1)?true:false);
	}
	highlighted = NULL;
	regenHuman = true;
}

void MyWindow::
cb_ceiling(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	ceiling = pSlider->value();

	seperation_consistency = false;
	Group::reset();
	Slice::reset();
	base_octree->deleteChild();
	int first = slice_idx - window_size;
	if ( first < 0 )
		first = 0;
	reset_timer();
	for ( int i = first ; i <= slice_idx ; i++ )
	{
		preprocessSlice(ppSlices[i]);
		generatePoints(ppSlices[i], (i==slice_idx-1)?true:false);
	}
	highlighted = NULL;
	regenHuman = true;
}

void MyWindow::
cb_highlight(Fl_Widget *widget, void *param)
{
	Fl_Value_Slider *pSlider = (Fl_Value_Slider *)widget;
	MyWindow *pWin = (MyWindow *)param;
	highlight = pSlider->value();

	OctreeEntryList::reset();
	if ( Slice::pLastSlice )
	{
		SliceEntity *pEntity = Slice::pLastSlice->entity[(int)highlight];
		if ( pEntity )
		{
			highlighted  = Slice::pLastSlice->entity[(int)highlight]->point;
			if ( highlighted )
			{
				double x, y, z;
				double coeff[3];
				double absval;
				
				x = highlighted->x;
				y = highlighted->y;
				z = highlighted->z;

				absval = sqrt(x*x + y*y + z*z);
				coeff[0] = x / absval;
				coeff[1] = y / absval;
				coeff[2] = z / absval;
				base_octree->getLineDistance(coeff, 0.01);
			}
		}
	}
	else
	{
		highlighted = NULL;
	}
}

void *seperate(void *)
{
	while (1)
	{
#if 1
		int i;
		int numGroup;
		bool dump_available;

		if ( !gSeperate )
		{
			sleep(1);
			continue;
		}

		pthread_mutex_lock(&mutex);
		GroupDump *dump = GroupDump::buffer;
		dump_available = ( dump != NULL && dump->seperated == false );
		pthread_mutex_unlock(&mutex);

		if ( !dump_available )
			continue;

		if ( !dump->sorted )
			dump->sort();

		dump->check();

		numGroup = dump->seperate(MERGE_THRESHOLD);
		dump->numGroup	= numGroup;

		pthread_mutex_lock(&mutex);
		dump->seperated = true;
		pthread_mutex_unlock(&mutex);
#endif
	}
}

struct packet
{
	long	index;		
	long    depth[1081];
	double  theta[1081];
};

void periodic_task(MySim *sim)
{
	static int counter = 0;
	static Group *trackingGroup = NULL;

	
	if ( !file_stream )
	{
		receive(NULL);
	}


	if ( counter%30 == 0 )
	{
		Group::log();
		Slice::log();
		Octree::log();
		if ( human )
			human->log();
		fprintf(stderr, "%d cube(s) drawn\n", drawn);
		if ( highlighted )
		{
			PointEntry *entry  = highlighted;
			fprintf(stderr, "Chosen cube group%d slice%d (%f,%f,%f)\n\n", entry->grp->id, (int)highlight,
				entry->tree->x, entry->tree->y, entry->tree->z);
		}
		if ( file_stream == false )
		{
			fprintf(stderr, "%d Slices are read\n", slice_idx);
		}

		if ( ts[0][1] > 0 )
		{
			fprintf(stderr, "Checking time %f(%d/%d) %f %f %f %f %f\n", 
					(double)ts[0][0]/ts[0][1], 
					ts[0][0],ts[0][1], 
					(double)ts[1][0]/ts[1][1], 
					(double)ts[2][0]/ts[2][1],
					(double)ts[3][0]/ts[3][1],
					(double)ts[4][0]/ts[4][1],
					(double)ts[5][0]/ts[5][1]);
		}

		trackingGroup = track();

		if ( trackingGroup )
		{
			double pos[3];
			trackingGroup->getPos(pos);
			trackingGroupIndex	= trackingGroup->index;
			trackingGroupId		= trackingGroup->id;
			trackPos[0]			= pos[0];
			trackPos[1]			= pos[1];
			trackPos[2]			= pos[2];

			if ( human_detection_idle == true )
			{
				if ( human_result )
				{
					if ( human )
						delete human;

					human = human_result;
					human_result = NULL;
				}
					
				if ( !human_request )
				{
					human_request = new HumanModel(*trackingGroup);
				}
			}

#if 0
		fprintf(stderr, " Body %f,%f,%f (%f,%f,%f)\n",
				human.body_pos(0), human.body_pos(1), human.body_pos(2),
				human.body_var(0,0), human.body_var(1,1), human.body_var(2,2));
		fprintf(stderr, " ARM %f,%f,%f (%f,%f,%f)\n",
				human.arm_pos[0], human.arm_pos[1], human.arm_pos[2],
				human.arm_var(0,0), human.arm_var(1,1), human.arm_var(2,2));
#endif
		}


		if ( gTrack && trackingGroup )
		{
			double var[3];
			trackingGroup->getVar(var);
			fprintf(stdout, "Tracking Pos [ %f %f %f ] (%f %f %f)\n", trackPos[0], trackPos[1], trackPos[2],
					var[0], 
					var[1],
					var[2] 
					);
			if ( sim )
			{
				Pos pos; 
				pos.x = trackPos[0];
				pos.y = trackPos[1];
				pos.z = trackPos[2];
				sim->trj.push_back(pos);
			}
			if ( pSendComm )
			{
				Message msg;

				msg.index		= counter;
				msg.size		= sizeof(double) * 3;
				msg.timeStamp 	= counter;
				msg.data		= (void *)trackPos; 

				pSendComm->send(&msg);
				fprintf(stderr, "Send Message\n");

			}
		}
	}

#if 1
	static int group_index = 0;

	int prev_index = group_index;

	bool dump_available;

	pthread_mutex_lock(&mutex);
	dump_available = Group::groupCount && GroupDump::buffer == NULL;
//		dump_available = false;
	pthread_mutex_unlock(&mutex);

	if ( dump_available )
	{
		int i;

		mark_start_time();
		for ( i = 1 ; i < Group::array_size ; i++ )
		{
			group_index = (prev_index+i)%Group::array_size;
			if ( Group::ppBaseGroup[group_index] && Group::ppBaseGroup[group_index]->numPoint >= group_threshold  )
				break;
		}
		
		if ( i < Group::array_size ) 
		{
			Group *group = Group::ppBaseGroup[group_index]; // Group::ppBaseGroup[group_index];

			if ( prev_index != group_index )
			{
//					int numGroup;

//						GroupDump dump0 = GroupDump(group);

//						if ( !group->sorted )
//							group->sort();

//					int usec = get_elapsed();
//					fprintf(stderr, "Sorting took %d usec\n", usec);

				pthread_mutex_lock(&mutex);
				GroupDump::buffer = new GroupDump(group);
				pthread_mutex_unlock(&mutex);
			}
		}
		seperation_consistency = true;
	}


	bool dump_ready;

	pthread_mutex_lock(&mutex);
	GroupDump *dump = GroupDump::buffer;
	dump_ready = dump && dump->seperated;
	pthread_mutex_unlock(&mutex);

	if ( dump_ready )
	{
		if ( seperation_consistency == true)
		{
			pthread_mutex_lock(&mutex);
			GroupDump::buffer = NULL;
			pthread_mutex_unlock(&mutex);

			mark_start_time();
			Group *group = Group::ppBaseGroup[dump->groupIndex];
			if ( group && group->id == dump->groupId && group->numPoint > 1 && dump->numGroup > 1 )
			{
				fprintf(stderr, "Seperate Group %d(%d) into %d groups\n", group->id, group_index, dump->numGroup);
				group->seperate(dump);
				group->check();
				int usec = get_elapsed();
				fprintf(stderr, "Seperation took %d usec\n", usec);
			}
		}

		delete dump;
		GroupDump::buffer = NULL;
		seperation_consistency = false;
	}
#endif

	counter++;
}

int main(int argc, char* argv[])
{
	int index, last_id;
	int i, n;
	data_stream ds;
	char *addr;
	bool isDaemon = false;
	pthread_mutex_init( &mutex, NULL );

	int c;
	file_stream = false;
	while ( (c = getopt(argc, argv, "f:c:dh") ) != -1 )
	{
		switch (c)
		{
		case 'f':
			if ( ds.open(optarg) == false )
				exit(-1);
			file_stream = true;
			break;
		case 'c':
			addr = optarg;
			break;
		case 'd':
			isDaemon = true;
			break;
		case 'h':
			cout << "Usage " << endl;
			cout << argv[0] << " -f file_name -c ip_address -d -h" << endl;
			cout << "   " << "f: point cloud log file" << endl;
			cout << "   " << "c: IP address where the tracking info is sent to" << endl;
			cout << "   " << "d: run as daemon" << endl;
			cout << "   " << "h: show this message" << endl;
			exit(0);
			break;
		}
	}

	if ( file_stream )
	{
		// Read Depth Data
		slice_t base_slice;
		slice_t *slice = &base_slice;
		last_id = -1;
		num_slice = 0;
//		double last_depth;
//		int seg_idx;
		vector<double> entry;

		while ( (n = ds.read_line()) > 0 )
		{
			double phi, theta, depth;
			entry = ds.get_entry();

			int id = (int)entry[0];	

			if ( id != last_id )
			{
				slice->pNext = (slice_t *)malloc(sizeof(slice_t));
				slice = slice->pNext;
				slice->pNext = NULL;
				slice->num = 0;
				slice->timestamp = (int)entry[1];
				slice->id = id;

				last_id = id;
				num_slice++;
//				seg_idx = 0;
//				last_depth = -10;
			}
			int num = slice->num;

			phi		= entry[2];
			theta	= entry[3];
			depth	= entry[4]/1000.;

			slice->x[num] = (double)depth * cos(theta) * sin(phi);
			slice->y[num] = (double)depth * sin(theta) * sin(phi);
			slice->z[num] = (double)depth * cos(phi);
			slice->phi[num]	= (double)phi;
			slice->theta[num]	= (double)theta;
			slice->depth[num]	= (double)depth;
//			if ( fabs(depth - last_depth) > MERGE_THRESHOLD * depth)
//			if ( fabs(depth - last_depth) > MERGE_THRESHOLD )
//				seg_idx++;	

//			slice->segment[num] = seg_idx;
			
#if 0
			if ( fabs(slice->x[num]) < 2. && fabs(slice->y[num]) < 2. && fabs(slice->z[num]) < 2. )
			{
				fprintf(stderr, "Slice (%f,%f,%f)\n", slice->x[num], slice->y[num], slice->z[num]);
			}
#endif

			slice->num++;
//			last_depth = depth;
		}
		ds.close();

		// Generate slice array
		slice = &base_slice;
		index = 0;
		ppSlices = (slice_t **)malloc(sizeof(slice_t*)*num_slice);
		while ( slice->pNext )
		{
			slice = slice->pNext;
			slice->id = index;
			
			ppSlices[index] = slice;
			index++;
		}
		fprintf(stderr, "File read..\n");
	}
	else
	{
		index = 0;
		ppSlices = (slice_t **)calloc(sizeof(slice_t*),1);

		pRecvComm = new Comm(51122, 20000);
		pRecvComm->listen();

	}

	if ( addr )
	{
		fprintf(stderr, "Sending socket opened...\n");
		pSendComm = new Comm(addr, CMD_PORT, 20000);
	}
	else
	{
		pSendComm = NULL;
	}


	Group::initialize();
	Slice::initialize();

#if 0
	base_octrees = (Octree **)malloc(sizeof(Octree) * num_slice);
	for ( i = 0 ; i < num_slice ; i++ )
	{
		base_octrees[i]			= new Octree();
		base_octrees[i]->x		= 0.;
		base_octrees[i]->y		= 0.;
		base_octrees[i]->z		= 0.;
		base_octrees[i]->len	= SPACE_SIZE;
	}
#else
	base_octree			= new Octree();
	base_octree->x		= 0.;
	base_octree->y		= 0.;
	base_octree->z		= 0.;
	base_octree->len	= SPACE_SIZE;
#endif
	seperation_consistency = false;
	Group::reset();
	Slice::reset();
	base_octree->deleteChild();
	for ( int i = 0 ; i <= slice_idx ; i++ )
	{
		preprocessSlice(ppSlices[i]);
		generatePoints(ppSlices[i], (i==slice_idx-1)?true:false);
	}
	highlighted = NULL;
	regenHuman = true;

	pthread_t seperating_thread;
	pthread_create( &seperating_thread, NULL, seperate, NULL);

	pthread_t body_detection_thread;
	pthread_create( &body_detection_thread, NULL, detectHumanBody, NULL);

#if 0
	if ( !file_stream )
	{
		pthread_t receiving_thread;
		pthread_create( &receiving_thread, NULL, receive, NULL);
	}
#endif

	int ret;

	if ( !isDaemon )
	{
		glutInit(&argc, argv);

		MyWindow win(win_width, win_height, win_title);

		ret = Fl::run();
	}
	else
	{
		while (1)
		{
			Group::log();
			Slice::log();
			Octree::log();
			periodic_task(NULL);

			sleep(1);
		}

		ret = 0;
	}
/*
	while ( myModel.z > 0 )
	{
		myModel.update();
		printf("x    : %5f, z    : %5f\n", myModel.x, myModel.z);
		printf("xdot : %5f, zdot : %5f\n", myModel.xdot, myModel.zdot);
		printf("xddot: %5f, zddot: %5f\n", myModel.xddot, myModel.zddot);
	}
*/
	
	return ret;
}

Group *track(void)
{
	double	min_distance2 = 1000*1000;
	int 	retIndex = 0;
	double	ref[3];
	Group	*ret = NULL;

	if ( gTrack )
	{
		Group *trackingGroup = Group::ppBaseGroup[trackingGroupIndex];

		if ( trackingGroup && trackingGroup->id == trackingGroupId && trackingGroup->numPoint > group_threshold )
		{
			return trackingGroup;
		}
		else
		{
			ref[0] = trackPos[0];
			ref[1] = trackPos[1];
			ref[2] = trackPos[2];
		}
	}
	else
	{
		ref[0] = 0.;
		ref[1] = 0.;
		ref[2] = -.2;
	}

	for ( int i = 0 ; i < Group::array_size ; i++ )
	{
		Group *group = Group::ppBaseGroup[i];
		double dx, dy, dz;
		double distance2;

		if ( !group )
			continue;

		assert(group->valid);
		double pos[3];

		group->getPos(pos);
		dx = pos[0] - ref[0];
		dy = pos[1] - ref[1];
		dz = pos[2] - ref[2];

		if ( group->isBase() )
			continue;

		// Ignore small group
		if ( group->numPoint < group_threshold )
			continue;

		distance2 = dx*dx + dy*dy + dz*dz;

		if ( distance2 < min_distance2 )
		{
			retIndex = group->index;
			min_distance2 = distance2;
		}
	}

	if ( retIndex > 0 )
	{
		Group *group = Group::ppBaseGroup[retIndex];
//		fprintf(stderr, "Tracking Group %d(%d) (%f,%d)\n", group->id, retIndex , min_distance2, group->numPoint);
		if ( min_distance2 < 0.1)
		{
			GroupEntity *entity = group->firstEntity->pNext;

			while ( entity != group->lastEntity )
			{
				PointEntry *point = entity->pPointEntry;	
//				fprintf(stderr, "%f,%f,%f ", point->x, point->y, point->z);
				entity = entity->pNext;
			}
//			fprintf(stderr, "\n");
		}

		ret = group;
	}

	return ret;
}

void *receive(void*)
{
	static int idx = 0;

	Message msg;
	slice_t *pSlice = (slice_t *)malloc(sizeof(slice_t));
	slice_idx = 0;
	static int old_index;
	int index;
	
	while (1) 
//	do
	{
		msg = pRecvComm->read();
		
		if ( msg.size == 0 )
//			continue;
			break;

		struct packet *pPkt = (struct packet *)msg.data;
		double prev_depth = -1000;
//		int 	segIdx = 0;


		pthread_mutex_lock(&mutex);

		index = pPkt->index;
		ppSlices[0] = pSlice;

		pSlice->id = idx+1;
		pSlice->num = 1081;

		if ( index < old_index || index > old_index+1 )
		{
			fprintf(stderr, "========================\n");
			fprintf(stderr, "ERROR INDEX %d - %d\n", index, old_index);
			fprintf(stderr, "========================\n");
		}
		for ( int i = 0 ; i < 1081 ; i++ )
		{
			double	phi, theta;
			double	depth = pPkt->depth[i] / 1000.;

			phi     =  (double)i / (double)1440 * 2 * M_PI  - 0.25 * M_PI;
			theta   =  pPkt->theta[i] * M_PI / 180.;

			pSlice->x[i] = (double)depth * cos(phi) * cos(theta);
			pSlice->y[i] = (double)depth * cos(phi) * sin(theta);
			pSlice->z[i] = (double)depth * sin(phi);
			pSlice->phi[i] = phi;
			pSlice->theta[i] = theta;
			pSlice->depth[i] = depth;
//			if ( abs(depth - prev_depth) > MERGE_THRESHOLD * depth )
//			if ( abs(depth - prev_depth) > MERGE_THRESHOLD )
//			{
//				segIdx++;
//			}
//			pSlice->segment[i] = segIdx;
		}
		idx++;
		
		preprocessSlice(pSlice);
		generatePoints(pSlice, false);

		int first = idx - window_size;
		if ( first > 0 )
		{
			Slice::deleteBefore(idx - window_size);
			base_octree->removeEmpty();
		}
		pthread_mutex_unlock(&mutex);

		old_index = index;
	}
}

void *detectHumanBody(void *)
{
	HumanModel *human_;
	while (1)
	{
		if ( human_request != NULL )
		{
			human_ = human_request;
			human_request = NULL;
		}
		else
		{
			usleep(10000);			
			continue;
		}
		human_detection_idle = false;

		human_->process();

		if ( human_result )
			delete human_result;

		human_result = human_;

		human_detection_idle = true;
	}
}

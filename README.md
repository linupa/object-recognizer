Object Recognizer
===================

Dependency
==========
1. FLTK
2. openGL
3. Eigen3


Running the Code
===========
    ./objrec -f file_name -c ip_address -d -h
        f: point cloud log file
        c: IP address where the tracking info is sent to
        t: IP address where the command is sent to
        d: run as daemon (experimental)
        h: show this message
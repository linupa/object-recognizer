#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <assert.h>

#include "Octree.h"
#include "DataStructure.h"

PointEntry::PointEntry(void)
{
    valid           = true;
	grp             = NULL;
    pGroupEntity    = NULL; 
	tree			= NULL;
    slices          = new SliceEntity;
	slices->pPrev = slices->pNext = NULL;
	contact			= false;
}

double PointEntry::distance(PointEntry *other)
{
	double ret;
	double dx, dy, dz;

	dx = tree->x - other->tree->x;
	dy = tree->y - other->tree->y;
	dz = tree->z - other->tree->z;

	ret = sqrt(dx*dx + dy*dy + dz*dz);
	
	return ret;
}

void PointEntry::absorb(OctreeEntry *other_)
{
	PointEntry *other = (PointEntry *)other_;
	
	if ( other == NULL )
		return; 
	
	if ( !grp )
	{
		pGroupEntity	= other->pGroupEntity;
		grp				= other->grp;
        pGroupEntity->pPointEntry = this;

        other->pGroupEntity = NULL;
        other->grp          = NULL;

        
	}
	else
	{
        // The absorbing entry is new instance which has no group yet
        assert(1);
#if 0
		Group::merge(grp, other->grp);
		if ( other->pGroupEntity->pNext )
			other->pGroupEntity->pNext->pPrev = other->pGroupEntity->pPrev;
		if ( other->pGroupEntity->pPrev )
			other->pGroupEntity->pPrev->pNext = other->pGroupEntity->pNext;
		delete other->pGroupEntity;
#endif
	}
//	fprintf(stderr, "One Occupancy Two Entry %08x %08x (%08x %08x)\n", this, other, pNext, pPrev);
	
//	other->slice->pPointEntry[other->sliceIndex] = NULL;

	delete other;
}

PointEntry::~PointEntry(void)
{
    valid = false;
    assert ( slices );

    SliceEntity *entity = slices;
    while ( entity )
    {
        SliceEntity *delSliceEntity = entity;
        entity = entity->pNext;
		delSliceEntity->point = NULL;
#ifdef SLICE_ENTITY_BUFFER
		delSliceEntity->~SliceEntity();
#else
        delete delSliceEntity;
#endif
    }

	if ( grp )
	{
#if 1
		Group::removePoint(this);
#else
		if ( pGroupEntity )
		{
			if ( pGroupEntity->pPrev )
				pGroupEntity->pPrev->pNext = pGroupEntity->pNext;
			if ( pGroupEntity->pNext )
				pGroupEntity->pNext->pPrev = pGroupEntity->pPrev;

			delete pGroupEntity;

			if ( grp->pEntity->pNext == NULL )
				delete grp;
	}
#endif
	}

	if ( tree )
	{
		tree->entry = NULL;
		tree->filled  = tree->filled & (~(1<<THIS_FILLED));
		tree->checkEmpty();
	}
}



#include <vector>
#include "GMM.h"
#include "KMeans.h"
#define SPACE_SIZE	(2.56)
#define MAX_DEPTH	(8)
#define MERGE_THRESHOLD (0.100) // Threshold per meter

#define DEFAULT_CEILING (1.7)
#define DEFAULT_FLOOR	(-.8)
#undef	SLICE_ENTITY_BUFFER
#undef	SURFACE_NORMAL

#define SLICE_SIZE		(1100)

#define GROUP_RESERVE	(10)

#define NUM_POINT 1081

using namespace std;

typedef struct slice
{
	int id;
	int timestamp;
	int num;
	bool valid;
	double x[NUM_POINT];
	double y[NUM_POINT];
	double z[NUM_POINT];
	double phi[NUM_POINT];
	double theta[NUM_POINT];
	double depth[NUM_POINT];
	int	segment[NUM_POINT];
	double corr[NUM_POINT];
	double weight[NUM_POINT];
	double dx[NUM_POINT];
	double dy[NUM_POINT];
	double dz[NUM_POINT];
	// Segment Information
	double a[NUM_POINT];
	double b[NUM_POINT];
	double distance[NUM_POINT];
	double size[NUM_POINT];
	double torque[3];
	struct slice *pNext;
} slice_t;

class PointEntry;
class Slice
{
public:
	int id;
	double torque[3];
	class SliceEntity *entity[SLICE_SIZE];
	class Slice *pNext;
	void addPoint(class PointEntry *pPoint, int index);

	static int count;
	static Slice *create(int index_);
	static Slice *pBaseSlice;
	static Slice *pLastSlice;
	static void reset(void);
	static void log(void);
	static void deleteBefore(int id);
	static void initialize(void);

private:
	Slice(void);
	~Slice(void); 
};

class SliceEntity
{
public:
    SliceEntity(void);
    ~SliceEntity(void);
    class Slice         *slice;
    int 				index;
	PointEntry 			*point;
	class SliceEntity    *pNext;
	class SliceEntity    *pPrev;
};

class GroupEntity
{
public:
	int id;
	class PointEntry *pPointEntry;
	class GroupEntity *pNext;
	class GroupEntity *pPrev;

	GroupEntity(class PointEntry *point);
	GroupEntity(void);
	GroupEntity(GroupEntity *src);
	~GroupEntity(void);

	static int last_id;
};

#define NUM_PARTICLE	(1000)
class GroupDump;
class Group
{
public:
	// Member Variables
	int		id;
	int		index;
	bool	valid;
	int		numPoint;
	class	GroupEntity *firstEntity;
	class	GroupEntity *lastEntity;
	bool	sorted;
	double	pos_sum[3];
	double	pos_ssum[3];
	double	px[NUM_PARTICLE];
	double	py[NUM_PARTICLE];
	double	pz[NUM_PARTICLE];
	double  pp[NUM_PARTICLE];
	double  pos_p[3];
	double  var_p[3];

	// Functions
	Group(void);
	Group(int id);
	Group(Group *src);
	void	init(void);
	~Group(void);
	void	addPoint(PointEntry *entry);
	int		seperate(GroupDump *dump);
	void	sort(void);
	bool	check(void);
	bool	isBase(void);
	void	generateParticles(Slice *pSlice);

	// Static Member Variables
	static Group **ppBaseGroup;
	static int groupCount;
	static int last_id;

	// Static Functions
	static void	removePoint(PointEntry *entry);
	static Group *merge(Group *grp1, Group *grp2);
	static Group *create(void);
	static void log(void);
	static void reset(void);
	static void initialize(void);
	static Group *reserved[GROUP_RESERVE];
	static void defineBase(void);
	static bool	isBase(PointEntry* entry);

	static int array_size;
	
	void getPos(double *pos_);
	void getVar(double *var_);

private:
	bool	dirty;
	double	pos[3];
	double	var[3];
	void	stat(void);
	void	setDirty(void);
};

class GroupDumpEntity
{
public:
	int id;
	int tag;
	double x;
	double y;
	double z;

	static double getDistance2(GroupDumpEntity *entity1, GroupDumpEntity *entity2);
};

class GroupDump
{
public:
	int		groupIndex;
	int		groupId;
	bool	sorted;
	bool	seperated;

	int				numPoint;
	int				numGroup;
	int				max_tag;
	unsigned int	*tag_mask;

	GroupDumpEntity *list;
	GroupDumpEntity **sortedList;

	GroupDump(Group *src);
	~GroupDump(void);
	static void swap(GroupDumpEntity *entity1, GroupDumpEntity *entity2);
	void sort(void);
	bool check(void);

	int seperate(double threshold);

	static GroupDump *buffer;
};

class PointEntry : public OctreeEntry
{
    bool valid; 
public:
	int timestamp;
	PointEntry(void);
	~PointEntry(void);

	Group	*grp;
	GroupEntity *pGroupEntity;
    class SliceEntity *slices; 
	double nx, ny, nz;
	bool contact;

	double distance(PointEntry *other);
	virtual void absorb(OctreeEntry *other);
};

class Point
{
public:
	double x, y, z;
	Point(double x, double y, double z);
};

class HumanModel
{
public:
	GMM *gmm;
	KMeans *hand;
	vector<Point> body;
	vector<Point> outlier;
	VectorXd body_pos;
	VectorXd arm_pos;
	MatrixXd body_var;
	MatrixXd arm_var;

	HumanModel(void);
	HumanModel(Group &group);
	~HumanModel(void);
	void process(void);
	static int iteration;
	static int creation;
};

ostream& operator<<(ostream& ost, const HumanModel &m);

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <vector>

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXcd;
using Eigen::MatrixXcd;


class Cluster
{
public:
	Cluster(int dimension);
	void update(void);

	int dimension;
	double pi;
	VectorXd mean;
	MatrixXd cov;
	VectorXcd eigval;
	MatrixXcd eigvec;
	double p(const VectorXd &x);
	VectorXd getMean();
	MatrixXd getCov();

private:
	MatrixXd cov_inv;
	double sqrt_cov_det;
};

class GMM
{
	public:
	GMM(int n_clust, int dim);
	~GMM(void);
	void reset(void);
	double iterate(void);
	double iterate(int times);
	double converge(double threshold, int trial);
	vector<Cluster> clusters;
	void setSamples(vector<VectorXd> *samples);
	void setDefaultClusters();
	void update(void);

	vector<VectorXd> *points;
	int iteration;
	double max_dist2;

	private:
	int dimension;
	vector<Cluster> default_clusters;
};

double	getRandomNormal(void);
ostream& operator<<(ostream& ost, const GMM& m);

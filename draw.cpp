
void drawOccupancy(Octree *octree)
{
	PointEntry *entry = (PointEntry *)(octree->entry);
	GLfloat clrTable[16][4] = {
		{1., 0., 0., 1.},
		{0., 1., 0., 1.},
		{0., 0., 1., 1.},
		{1., 1., 0., 1.},
		{1., 0., 1., 1.},
		{0., 1., 1., 1.},
		{1., 1., 1., 1.},
		{1., 0., .5, 1.},
		{1., .5, .0, 1.},
		{0., 1., .5, 1.},
		{.5, 1., 0., 1.},
		{0., .5, 1., 1.},
		{.5, 0., 1., 1.},
		{1., .5, .5, 1.},
		{.5, 1., .5, 1.},
		{5., .5, 1., 1.}
	};
	if ( entry && entry->grp->numPoint > group_threshold ) //&& entry->grp)
	{
		int idx;

		if ( gTrack && entry->grp->id == trackingGroupId )
			idx = 0;
		else
			idx = (entry->grp->id)%15 + 1;
		glPushMatrix();
	//	glRotatef(thdeg[0], 0., 0., 1.);
	//	glMaterialfv(GL_FRONT, GL_DIFFUSE, clrTable[idx]);
		glColor3f(clrTable[idx][0], clrTable[idx][1], clrTable[idx][2]);
		glTranslatef(octree->x, octree->y, octree->z);
		drawBlock(octree->len*2, octree->len*2);
		glPopMatrix();
		drawing++;
	}
#if 0
	else
	{
		glPushMatrix();
	//	glRotatef(thdeg[0], 0., 0., 1.);
		glColor3f(.5,.5,.5);
		glTranslatef(octree->x, octree->y, octree->z);
		drawBlock(octree->len*2, octree->len*2);
		glPopMatrix();
	}
#endif
	
}

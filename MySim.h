#ifndef _MY_SIM_H_
#define _MY_SIM_H_

#include <vector>

#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl.H>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <FL/fl_draw.H>

using namespace std;

typedef struct {
	double x;
	double y;
	double z;
} Pos;

extern double	high_z;
extern double	farthest;
extern double	min_rad, max_rad;

class MySim : public Fl_Gl_Window
//class MySim : public Fl_Widget
{
public:
	MySim(int xx, int yy, int width, int height);
	void initializeGl(void);
	virtual ~MySim();
    bool firstTime;
	vector<Pos>trj;
	
	virtual void draw();
	virtual void resize(int x, int y, int w, int h);
	virtual void show();

	void tick();
	static void timer_cb(void *param);
	
#if 1
	int handle(int e);
#endif
};

class MyStat : public Fl_Widget
{
public:
	MyStat(int xx, int yy, int width, int height);
	virtual ~MyStat();
	
	virtual void draw();

	void tick();
	static void timer_cb(void *param);
	
	int handle(int e);
};

#endif // _MY_SIM_H_

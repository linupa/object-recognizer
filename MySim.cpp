#include <stdio.h>
#include <math.h>

#include <Eigen/Dense>

#include "Octree.h"
#include "MySim.h"
#include "DataStructure.h"
#include "Comm.h"

#define CLICK_THRESHOLD 3

using Eigen::MatrixXd;

double target_pos[2];
int point_pressed = -1;
int drawing;
double hor = 0;
double ver = 0;

extern int drawn;
extern double dt;
double x_org;
double y_org;
extern double scale;
extern int	group_threshold;
extern bool gTrack;
extern bool gShowBase;
extern int trackingGroupIndex;
extern int trackingGroupId;
extern double trackPos[3];
extern Octree *base_octree;
extern PointEntry *highlighted;
extern slice_t **ppSlices;
extern int slice_idx;
extern HumanModel *human;
extern double gTorque[3];
extern double phi;
extern Comm *pCmdComm;
extern int cmd;


//MySim::MySim(int xx, int yy, int width, int height) : Fl_Widget(xx, yy, width, height, "")
MySim::MySim(int xx, int yy, int width, int height) : Fl_Gl_Window(xx, yy, width, height, "")
{
	fprintf(stderr, "Gl Window created\n");
//	mode(FL_RGB | FL_ALPHA | FL_DEPTH | FL_DOUBLE);

	Fl::add_timeout(dt, timer_cb, this);
	firstTime = true;
	trj.clear();
}

void MySim::initializeGl(void)
{
	glClearColor( 0., 0., 0., 0.);

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

#if 0
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	GLfloat diff[] = {1., 1., 1., 1.};
	GLfloat ambi[] = {.9, .9, .9, 1.};
	GLfloat spec[] = {0., 0., 1., 1.};	

	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diff);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambi);
	glLightfv(GL_LIGHT0, GL_SPECULAR, spec);
#endif

//	glLightfv(GL_LIGHT1, GL_AMBIENT, ambi);
}

void MySim::show()
{
//	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	firstTime = true;

	Fl_Gl_Window::show();
}

void MySim::resize(int x, int y, int w, int h)
{
	int width, height;
	width = w, height = h;
	Fl_Gl_Window::resize(x, y, width, height);
//	Fl_Widget::resize(x, y, width, height);
//	if ( !firstTime )
		glViewport(0.,0., width, height);
}
MySim::~MySim()  {
	Fl::remove_timeout(timer_cb, this);

}

int MySim::handle(int e)
{
	int ret = 0;
	int i;
	double posX, posY;
	static double x_pressed, y_pressed;
	static double org_hor, org_ver;

	posX = Fl::event_x();
	posY =  Fl::event_y();


	switch (e)
	{
	case FL_PUSH:
		x_pressed = posX;
		y_pressed = posY;
		org_hor = hor;
		org_ver = ver;
		target_pos[0] = (posX - x_org) / (scale+0.1);
		target_pos[1] = (y_org - posY) / (scale+0.1);
		point_pressed = -1;
		for ( i = 0 ; i < 4 ; i++ )
		{
			double diffx, diffy;
//				diffx = target_pos[0] - myModel.p(i,0);
//				diffy = target_pos[1] - myModel.p(i,1);
			if ( diffx*diffx + diffy*diffy < CLICK_THRESHOLD*CLICK_THRESHOLD/scale/scale )
			{
				point_pressed = i;
				break;
			}
		}
		fprintf(stderr, "PUSH EVENT!!! (%d)(%f,%f)\n", 
			point_pressed, target_pos[0], target_pos[1]);
		ret = 1;
		break;
	case FL_DRAG:
		fprintf(stderr, "DRAG EVENT!!! (%f,%f) -> (%f,%f)\n", x_pressed, y_pressed, posX, posY);
//		if ( point_pressed >= 0 )
		{
//				myModel.p(point_pressed, 0) = (posX - x_org) / scale;
//				myModel.p(point_pressed, 1) = (y_org - posY) / scale;
			hor = org_hor - (posX - x_pressed) * 0.01;
			ver = org_ver + (posY - y_pressed) * 0.01;

			if ( ver > M_PI/2. )
				ver = M_PI/2.;
			else if ( ver < -M_PI/2. )
				ver = -M_PI/2.;
		}
		
		break;
	case FL_RELEASE:
		fprintf(stderr, "RELEASE EVENT!!!\n");
		point_pressed = -1;
		break;
	case FL_ENTER:
		fprintf(stderr, "FOCUS EVENT!!!\n");
		ret = 1;
		break;
	case FL_MOUSEWHEEL:
		fprintf(stderr, "MOUSE WHEEL!!!\n");
		scale += Fl::event_dy()/10.;

		if ( scale < 0. )
			scale = 0.;
		ret = 1;
		break;
	}
	Fl_Gl_Window::handle(e);
//		Fl_Widget::handle(e);

	return ret;
}

// OpenGL Drawing
void drawBlock(float width, float height)
{
	float h_w = width/2.;
	glBegin(GL_QUADS);

	glNormal3d(0,0,-1);
	glVertex3f(-h_w, -h_w, 0);
	glVertex3f(-h_w, h_w, 0);
	glVertex3f(h_w, h_w, 0);
	glVertex3f(h_w, -h_w, 0);

	glNormal3d(0,0,1);
	glVertex3f(-h_w, -h_w, height);
	glVertex3f(h_w, -h_w, height);
	glVertex3f(h_w, h_w, height);
	glVertex3f(-h_w, h_w, height);

	glNormal3d(0,1,0);
	glVertex3f(-h_w, h_w, 0);
	glVertex3f(-h_w, h_w, height);
	glVertex3f(h_w, h_w, height);
	glVertex3f(h_w, h_w, 0);

	glNormal3d(0,-1,0);
	glVertex3f(-h_w, -h_w, 0);
	glVertex3f(h_w, -h_w, 0);
	glVertex3f(h_w, -h_w, height);
	glVertex3f(-h_w, -h_w, height);

	glNormal3d(1,0,0);
	glVertex3f(h_w, -h_w, 0);
	glVertex3f(h_w, h_w, 0);
	glVertex3f(h_w, h_w, height);
	glVertex3f(h_w, -h_w, height);

	glNormal3d(-1,0,0);
	glVertex3f(-h_w, -h_w, 0);
	glVertex3f(-h_w, -h_w, height);
	glVertex3f(-h_w, h_w, height);
	glVertex3f(-h_w, h_w, 0);

	glEnd();
}
static int draw_count = 0;
void drawOccupancy(Octree *octree)
{
	PointEntry *entry = (PointEntry *)(octree->entry);
	GLfloat clrTable[16][4] = {
		{1., 0., 0., 1.},
		{1., 1., 1., 1.},
		{0., 0., 1., 1.},
		{0., 1., 0., 1.},
		{1., 1., 0., 1.},
		{1., 0., 1., 1.},
		{0., 1., 1., 1.},
		{1., 0., .5, 1.},
		{1., .5, .0, 1.},
		{0., 1., .5, 1.},
		{.5, 1., 0., 1.},
		{0., .5, 1., 1.},
		{.5, 0., 1., 1.},
		{1., .5, .5, 1.},
		{.5, 1., .5, 1.},
		{5., .5, 1., 1.}
	};
	if ( !entry || entry->grp->numPoint < group_threshold ) //&& entry->grp)
		return;

	if ( entry->grp->isBase() && !gShowBase )
		return;

	int idx;

//	if ( gTrack && entry->grp->id == trackingGroupId )
	if ( entry->grp->isBase() )
	{
		idx = 0;
	}
	else if ( entry->contact )
	{
		idx = 1;
		
//		if ( !(draw_count % 30) )
//		{
//			fprintf(stdout, "CONTACT: %f %f %f \n", octree->x , octree->y, octree->z);
//		}
	}
	else if ( gTrack && entry->grp->id == trackingGroupId )
	{
		idx = 2;
	}
	else
		idx = (entry->grp->id)%13 + 3;

#if 1
	if ( gTrack && idx > 2 )
		return;
#endif

	glPushMatrix();
//	glRotatef(thdeg[0], 0., 0., 1.);
//	glMaterialfv(GL_FRONT, GL_DIFFUSE, clrTable[idx]);
	glColor3f(clrTable[idx][0], clrTable[idx][1], clrTable[idx][2]);
	glTranslatef(octree->x, octree->y, octree->z);
	drawBlock(octree->len*2, octree->len*2);
	glPopMatrix();
	drawing++;

#ifdef SURFACE_NORMAL
	// Show Surface Normal
	glPushMatrix();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	glVertex3f(octree->x, octree->y, octree->z);
	glVertex3f(octree->x+entry->nx/10., octree->y+entry->ny/10., octree->z+entry->nz/10.);
	glEnd();
	glPopMatrix();
#endif

#if 0
	else
	{
		glPushMatrix();
	//	glRotatef(thdeg[0], 0., 0., 1.);
		glColor3f(.5,.5,.5);
		glTranslatef(octree->x, octree->y, octree->z);
		drawBlock(octree->len*2, octree->len*2);
		glPopMatrix();
	}
#endif
	
}

#define RESOLUTION	(20)
void drawEllipsoid(const VectorXd pos, const MatrixXd var)
{
	int i, j;
	double x, y, z, cx, cy, cz;
	double sx, sy, sz;
	double rx, ry, rz;

	cx = pos[0];
	cy = pos[1];
	cz = pos[2];
	sx = sqrt(var(0,0));
	sy = sqrt(var(1,1));
	sz = sqrt(var(2,2));

	x = cx;
	y = cy;
	for ( i = 0 ; i < RESOLUTION ; i++ )
	{
		double ratio;
		ratio = (double)(2.*i - (RESOLUTION))/RESOLUTION;
		z = cz + sz * ratio;
		rx = sx * sqrt(1.-ratio*ratio);
		ry = sy * sqrt(1.-ratio*ratio);
		for ( j = 0 ; j < RESOLUTION ; j++ )
		{
			double theta0, theta1;

			theta0 = 2.*M_PI*j/(double)RESOLUTION;
			theta1 = 2.*M_PI*(j+1)/(double)RESOLUTION;
			glPushMatrix();
			glBegin(GL_LINES);
			glVertex3f(x + rx*cos(theta0), y + ry*sin(theta0), z );
			glVertex3f(x + rx*cos(theta1), y + ry*sin(theta1), z );
			glEnd();	
			glPopMatrix();
		}
	}
	x = cx;
	z = cz;
	for ( i = 0 ; i < RESOLUTION ; i++ )
	{
		double ratio;
		ratio = (double)(2.*i - (RESOLUTION))/RESOLUTION;
		y = cy + sy * ratio;
		rx = sx * sqrt(1.-ratio*ratio);
		rz = sz * sqrt(1.-ratio*ratio);
		for ( j = 0 ; j < RESOLUTION ; j++ )
		{
			double theta0, theta1;

			theta0 = 2.*M_PI*j/(double)RESOLUTION;
			theta1 = 2.*M_PI*(j+1)/(double)RESOLUTION;
			glPushMatrix();
			glBegin(GL_LINES);
			glVertex3f(x + rx*cos(theta0), y, z + rz*sin(theta0) );
			glVertex3f(x + rx*cos(theta1), y, z + rz*sin(theta1) );
			glEnd();	
			glPopMatrix();
		}
	}
}

void drawCluster(Cluster &cluster)
{
	int i, j, k;
	double x, y, z, cx, cy, cz;
	double sx, sy, sz;
	double rx, ry, rz;

	cx = cluster.mean[0];
	cy = cluster.mean[1];
	cz = cluster.mean[2];
	sx = cluster.cov(0,0); 
	sy = cluster.cov(1,1);
	sz = cluster.cov(2,2);

	x = cx;
	y = cy;

	double l;

	double sign_map[4][2] = 
	{
		{-1., -1.},
		{ 1., -1.},
		{-1.,  1.},
		{ 1.,  1.}
	};
	glLineWidth(5.0);
	glPushMatrix();
	glTranslatef(cx, cy, cz);
	for ( i = 0 ; i < 3 ; i++ )
	{
		double l1 = cluster.eigval(i).real();;
		l1 = sqrt(l1);
		for ( j = i+1 ; j < 3 ; j++ )
		{
			double l2 = cluster.eigval(j).real();;
			l2 = sqrt(l2);

			for ( k = 0 ; k < 4 ; k++ )
			{
				glBegin(GL_LINES);
				l = sqrt(l);
				glVertex3f(
						l1*cluster.eigvec.col(i)[0].real() * sign_map[k][0], 
						l1*cluster.eigvec.col(i)[1].real() * sign_map[k][0], 
						l1*cluster.eigvec.col(i)[2].real() * sign_map[k][0]);
				glVertex3f(
						l2*cluster.eigvec.col(j)[0].real() * sign_map[k][1], 
						l2*cluster.eigvec.col(j)[1].real() * sign_map[k][1], 
						l2*cluster.eigvec.col(j)[2].real() * sign_map[k][1]);
				glEnd();
			}
		}
	}
	glPopMatrix();
	glLineWidth(1.0);

}

extern double _center;
extern double	avg[2];
extern double	min_pos[2], max_pos[2];

#define X(x) (x_org + (x)*scale)
#define Y(y) (y_org - (y)*scale)
  void MySim::
  draw()
  {
	int i, j;
	double xx, yy, zz;
//	xx = myModel.getX();
//	zz = myModel.getZ();
	MatrixXd p[4];
	double curv;
	GLfloat red[] = {1., 0., 0., 1.};
	GLfloat white[] = {1., 1., 1., 1.};


#if 0
    if (w() > h()) {
      scale = h() / 12.0;
    }
    else {
      scale = w() / 12.0;
    }
#endif

	double width, height;

	width = w();
	height = h();
    x_org = width / 4.0;
    y_org = height / 4.0;

#if 1
	if (firstTime)
	{
		fprintf(stderr, "GL Initialized\n");
		initializeGl();
		firstTime = false;
	}// if

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);      // clear the color and depth buffer

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1, 1, -height/width, height/width, 1, 10);
//	glFrustum(-2, 2, -2, 2, 0, 4);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt((scale+1)*cos(hor)*cos(ver), (scale+1)*sin(hor)*cos(ver), (scale+1)*sin(ver), 0, 0, 0, 0,0,1);
//	gluLookAt((scale+4), 0, 1., 0, 0, 0, 1./(scale+4), 0, 1);

	GLfloat lightPos[] = {1.0, 0., 1., 0.};
	GLfloat lightDir[] = {1.0, 0., 0.};
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLineWidth(1.0);
//	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDir);

	// Draw a sphere on the center of the coordinate
#if 0
	glPushMatrix();
//	glRotatef(thdeg[0], 0., 0., 1.);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
	glColor3f(1., 1., 1.);
//	drawBlock(0.05, 0.05);
	glutSolidSphere(0.025,20,20);
	glPopMatrix();
#endif

	if ( !gTrack )
	{
	// Show the orientation of the mobile platform
	glLineWidth(3.0);
	for ( i = 0 ; i < 3 ; i++ )
	{
		double val[3][3] = {{1.,0.,0.}, {0., 1., 0.}, {0., 0., 1.}};
		double x, y, z;

		x = val[i][0]*cos(phi) - val[i][1]*sin(phi);
		y = val[i][0]*sin(phi) + val[i][1]*cos(phi);
		z = val[i][2];
		glColor3f(val[i][0], val[i][1], val[i][2]);
		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3f(0., 0., 0.);
		glVertex3f(x*.1, y*.1, z*.1);
		glEnd();
		glPopMatrix();
	}
	}

	// Draw Occupancy map
    drawing = 0;
	glLineWidth(1.0);
	base_octree->callBack(drawOccupancy);
    drawn = drawing;

	draw_count++;

	// Highlight the specified voxel
	if ( highlighted )
	{
		Octree *octree = highlighted->tree;
		if ( octree )
		{
			glPushMatrix();
			//	glRotatef(thdeg[0], 0., 0., 1.);
			//	glMaterialfv(GL_FRONT, GL_DIFFUSE, clrTable[idx]);
			glColor3f(1., 1., 1.);
			glTranslatef(octree->x, octree->y, octree->z);
	//			drawBlock(octree->len*2, octree->len*2);
			drawBlock(0.05, 0.05);
			glPopMatrix();

			glBegin(GL_LINES);
			glVertex3f(0., 0., 0.);
			glVertex3f(octree->x, octree->y, octree->z);
			glEnd();
		}

		OctreeEntryList *neighbor = OctreeEntryList::list;
		for ( i = 0 ; i < OctreeEntryList::count ; i++, neighbor++ )
		{
			if ( neighbor->pEntry )
			{
				Octree *octree = neighbor->pEntry->tree;
				glPushMatrix();
				//	glRotatef(thdeg[0], 0., 0., 1.);
				//	glMaterialfv(GL_FRONT, GL_DIFFUSE, clrTable[idx]);
				glColor3f(1., 0., 0.);
				glTranslatef(octree->x, octree->y, octree->z);
		//			drawBlock(octree->len*2, octree->len*2);
				drawBlock(0.05, 0.05);
				glPopMatrix();
			}
		}
	}

	Group *group = Group::ppBaseGroup[trackingGroupIndex];
	if ( group && group->id == trackingGroupId )
	{
		double pos[3];
		group->getPos(pos);
		trackPos[0] = pos[0];
		trackPos[1] = pos[1];
		trackPos[2] = pos[2];

		// The center of the tracking object (Mean)
#if 1
		glPushMatrix();
		glTranslatef(trackPos[0], trackPos[1], trackPos[2]);
		glColor3f(1., 0.5, 0.5);
		glutSolidSphere(0.05,20,20);
		glPopMatrix();
#endif

		// The center of the tracking object (Particle filter)
#if 0
		glPushMatrix();
		glTranslatef(group->pos_p[0], group->pos_p[1], group->pos_p[2]);
		glColor3f(1., 1., 0.5);
		glutSolidSphere(0.05,20,20);
		glPopMatrix();
#endif

		// Line from the mean position to particel filter position
#if 0
		glBegin(GL_LINES);
		glTranslatef(trackPos[0], trackPos[1], trackPos[2]);
		glVertex3f(group->pos_p[0], group->pos_p[1], group->pos_p[2]);
		glEnd();
#endif

		// Draw Particles
#if 0
		for ( int i ; i < NUM_PARTICLE ; i++ )
		{
			double tone = group->pp[i];
			if ( tone > 1. )
			{
				glColor3f(1., 0.0, .0);
			}
			else
			{
				glColor3f(0., 1., 0.);
			}
			glBegin( GL_POINTS);
			glVertex3f( group->px[i],group->py[i],group->pz[i]);
			glEnd();
		}
#endif

		// Draw Trajectory of tracking object
#if 0
		if ( gTrack && trj.size() > 0 )
		{
			vector<Pos>::iterator it = trj.begin();
			Pos prev;

			prev = *it;
			it++;

			for ( ; it != trj.end() ; it++ ) 
			{
				Pos curr = *it;
				glPushMatrix();
				glLineWidth(5.0);
				glColor3f(1.0, 1.0, 1.0);
				glBegin(GL_LINES);
				glVertex3f(prev.x, prev.y, prev.z);
				glVertex3f(curr.x, curr.y, curr.z);
				glEnd();
				glPopMatrix();
				prev = *it;
			}
		}
#endif

		double rx, ry, rz, cx, cy, cz, x, y, z, sx, sy, sz;
		double var[3];

		group->getVar(var);
		cx = trackPos[0];
		cy = trackPos[1];
		cz = trackPos[2];
		sx = sqrt(var[0]);
		sy = sqrt(var[1]);
		sz = sqrt(var[2]);

		glColor3f(1., 0.5, 0.5);
#if 0
		{
			double pos[3], _var[3];
			pos[0] = cx, pos[1] = cy, pos[2] = cz;
			_var[0] = var[0]*4.;
			_var[1] = var[1]*4.;
			_var[2] = var[2]*4.;
			drawEllipsoid(pos, _var);
		}
#endif

#if 1
//		if ( human.outlier.size() && human.arm_var(0,0)+human.arm_var(1,1)+human.arm_var(2,2) > 0. )

#if 0
		if ( human.outlier.size() > 0 )
		{
			vector<Point>::iterator it;

			glColor3f(1.,0.0,0.0);
			for ( it = human.outlier.begin() ; it != human.outlier.end() ; it++ )
			{
				glPushMatrix();
				glTranslatef(it->x, it->y, it->z);
				drawBlock(0.02, 0.02);
				glPopMatrix();
			}
		}
#endif
#endif
	}

	// Contact Force Estimation (Net Force)
	MatrixXd	Jcw;
	VectorXd	torque = VectorXd(3);
	VectorXd	force;
	MatrixXd	H;

	#define R (.3)
	#define r (.1)
	Jcw = MatrixXd::Zero(3,3);
	Jcw(0,0) = -sin(phi);
	Jcw(0,1) = cos(phi);
	Jcw(0,2) = R;
	Jcw(1,0) = -sin(phi+2*M_PI/3);
	Jcw(1,1) = cos(phi+2*M_PI/3);
	Jcw(1,2) = R;
	Jcw(2,0) = -sin(phi-2*M_PI/3);
	Jcw(2,1) = cos(phi-2*M_PI/3);
	Jcw(2,2) = R;
	Jcw		= Jcw / r;

	torque[0] = -gTorque[0];
	torque[1] = -gTorque[1];
	torque[2] = -gTorque[2];

	force	= Jcw.transpose() * torque * 0.03;

//	fprintf(stderr, "%f %f %f %f %f %f\n", torque[0], torque[1], torque[2], force[0], force[1], force[2]); 
	// Draw Human Posture
	int numBody		= 0;
	int	numContact	= 0;
	if ( human )
	{
		numBody		= human->gmm->clusters.size();
		numContact	= human->hand->clusters.size();

		glColor3f(0.5, 1., 0.5);
		for ( i = 0 ; i < numBody ; i++ )
		{
			drawCluster(human->gmm->clusters[i]);
//			fprintf(stderr, "ITERATION: %d\n", human->gmm->iteration);
		}

		glColor3f(1.0, 1.0, 1.0);
//		drawCluster(human->hand->clusters[0]);
//		drawCluster(human->hand->clusters[1]);
		if ( human->hand->points->size() )
		{
			for ( i = 0 ; i < numContact ; i++ )
				drawEllipsoid(human->hand->clusters[i].mean, MatrixXd::Identity(3,3)/300);
		}
	}		

	if ( numContact > 0 )
	{
		VectorXd Fn;
		double mag[10];
		double theta[10];
		double deg[10];

		H  = MatrixXd::Zero(3, numContact * 2 );

		for ( i = 0 ; i < numContact ; i++ )
		{
			H(0,i*2)	= 1;
			H(1,i*2+1)	= 1;
			H(2,i*2)	= -human->hand->clusters[i].mean[1];
			H(2,i*2+1)	= human->hand->clusters[i].mean[0];
		}

		if ( H.rows() < H.cols() )
			Fn = H.transpose() * (H*H.transpose()).inverse() * force;
		else
			Fn = (H.transpose()*H).inverse()* H.transpose() * force;

		glColor3f(1.0, .5, .5);
		glLineWidth(8.0);
		for ( i = 0 ; i < numContact ; i++ )
		{
			VectorXd mean = human->hand->clusters[i].mean;

			theta[i] = atan2(Fn(2*i+1), Fn(2*i));
			deg[i] = theta[i]*180./M_PI;
			mag[i] = sqrt(Fn(2*i+1)*Fn(2*i+1) + Fn(2*i)*Fn(2*i));
			theta[i] -= phi;

			glColor3f(1.0, 1.0, .5);
			glPushMatrix();
			glTranslatef(mean[0], mean[1], mean[2]);
			glRotatef(deg[i], 0., 0., 1.);
			glBegin(GL_LINES);
			glVertex3f(0., 0., 0.);
			glVertex3f(mag[i], 0., 0.);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(mag[i], 0., 0.);
			glVertex3f(mag[i]-0.1, -0.1, 0.);
			glEnd();
			glBegin(GL_LINES);
			glVertex3f(mag[i], 0., 0.);
			glVertex3f(mag[i]-0.1, +0.1, 0.);
			glEnd();
			glPopMatrix();

			glColor3f(1.0, 1.0, 1.);
			glPushMatrix();
			glTranslatef(mean[0], mean[1], mean[2]);
			glBegin(GL_LINES);
			glVertex3f(0., 0., 0.);
			glVertex3f(Fn(2*i), Fn(2*i+1), 0.);
			glEnd();
			glPopMatrix();
//			fprintf(stderr, "%f %f\n", Fn(2*i), Fn(2*i+1));
		}

		double trackDist;
		double dist[2];

		dist[0] = trackPos[0] + 0.1;
		dist[1] = trackPos[1];

		trackDist = sqrt(dist[0]*dist[0] + dist[1]*dist[1]);


//		fprintf(stderr, "mag %f %f\n", mag[0], trackDist);
		switch ( numContact )
		{
			case 1:
				if ( mag[0] > 0.35 )
				{
					if ( theta[0] < 0. )
						theta[0] += 2*M_PI;

					if ( trackDist < 0.5 )
						cmd = 5;
					else if ( theta[0] < M_PI/4. || theta[0] > 7./4.*M_PI )
						cmd = 1;
					else if ( theta[0] > M_PI*3/4. || theta[0] < 5./4.*M_PI )
						cmd = 2;
				}
				break;
			case 2:
				{
					if ( mag[0] > 0.35 && mag[1] > 0.35 && (cos(theta[0] - theta[1]) < 0.) )
					{
						if ( force[2] > 0.)
						{
							cmd = 3;
						}
						else
							cmd = 4;
					}
				}
				break;
		}

		if ( cmd )
		{
			fprintf(stderr, "CMD %d  Theta:%f Mag:%f TrackDist: %f\n", cmd, theta[0], mag[0], trackDist);
		}
	}
	else
	{
		glColor3f(1.0, .5, .5);

		glLineWidth(3.0);
		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3f(0., 0., 0.);
		glVertex3f(force[0], force[1], 0.);
		glEnd();
		glPopMatrix();
	}


#if 0
	for ( i = 0 ; i < 3 ; i++ )
	{
		double mag = force[i];
		double vec[3][3] = { 	{ 1., 0., 0. },
								{ -.5, sqrt(3.)/2., 0.},
								{ -.5, -sqrt(3.)/2., 0.}};

		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3f(0., 0., 0.);
		glVertex3f(vec[i][0]*mag, vec[i][1]*mag, vec[i][2]*mag);
		glEnd();
		glPopMatrix();
	}
#endif
	

	glLineWidth(1.0);
	if ( !gShowBase )
	{
		double x,y,z;
		double min_rad, max_rad;

		min_rad = atan2(min_pos[1], min_pos[0]);

		x = farthest * cos(min_rad);
		y = farthest * sin(min_rad);
		z = high_z;

		glPushMatrix();
		glLineWidth(5.0);
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);
		glVertex3f(0., 0., z);
		glVertex3f(x, y, z);
		glEnd();
		glPopMatrix();

		max_rad = atan2(max_pos[1], max_pos[0]);
		x = farthest * cos(max_rad);
		y = farthest * sin(max_rad);
		z = high_z;

		glPushMatrix();
		glLineWidth(5.0);
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);
		glVertex3f(0., 0., z);
		glVertex3f(x, y, z);
		glEnd();
		glPopMatrix();

		if ( max_rad < min_rad )
			max_rad += 2*M_PI;
#define ARC_RES (20)
		double step = (max_rad - min_rad) / (double)ARC_RES;

		for ( int i = 0 ; i < ARC_RES ; i++ )
		{
			double p0[2], p1[2];

			p0[0] = farthest * cos(min_rad + step*i);
			p0[1] = farthest * sin(min_rad + step*i);
			p1[0] = farthest * cos(min_rad + step*(i+1));
			p1[1] = farthest * sin(min_rad + step*(i+1));
			glPushMatrix();
			glBegin(GL_LINES);
			glVertex3f(p0[0], p0[1], z);
			glVertex3f(p1[0], p1[1], z);
			glEnd();
			glPopMatrix();
		}
	}

	glPushMatrix();
	glRotatef(phi*180./M_PI, 0., 0., 1.);
	for ( i = -10 ; i <= 10 ; i++ )
	{
		glLineWidth(1.0);
		glColor3f(1., 1., 1.);

		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3f(i*0.1, -1., -0.8);
		glVertex3f(i*0.1, 1., -0.8);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glBegin(GL_LINES);
		glVertex3f(-1., i*0.1, -0.8);
		glVertex3f(1., i*0.1, -0.8);
		glEnd();
		glPopMatrix();
	}
	glPopMatrix();
	// Test code ... unused
	if (0)
	{
		glPushMatrix();
		glLineWidth(2.0);
		glColor3f(1., .0, 0.);
		glBegin(GL_LINES);
		glVertex3f(.0, 0., 0.);
		glVertex3f(.1, 0., 0.);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glLineWidth(2.0);
		glColor3f(0.0, 1.0, 0.0);
		glBegin(GL_LINES);
		glVertex3f(0., 0., 0.);
		glVertex3f(0., .1, 0.);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glLineWidth(2.0);
		glColor3f(0.0, 0.0, 1.0);
		glBegin(GL_LINES);
		glVertex3f(0., 0., 0.);
		glVertex3f(0., .0, 0.1);
		glEnd();
		glPopMatrix();
	}

#else // Unused parts
    fl_color(FL_WHITE);
    fl_rectf(x(), y(), w(), h());

    fl_color(FL_RED);
	slice_t *slice = ppSlices[slice_idx];
	for ( i = 0 ; i < slice->num ; i++ )
	{
		int color = i*255/slice->num;
		
#if 0
		xx = 100. * slice->x[i];
		yy = 100. * slice->y[i];	
#endif
		xx = 100. * sqrt(slice->x[i]*slice->x[i] + slice->y[i]*slice->y[i]);
		yy = -100. * slice->z[i];	
		fl_color(color,color,color);
		fl_line(x_org, y_org, x_org + xx, y_org + yy);
	}

#endif

    glCullFace(GL_BACK);
  }

MyStat::MyStat(int xx, int yy, int width, int height) : Fl_Widget(xx, yy, width, height, "")
{
	fprintf(stderr, "Fl Widget created\n");
	Fl::add_timeout(dt, timer_cb, this);
}

MyStat::~MyStat()
{
	Fl::remove_timeout(timer_cb, this);
}

#define SET_SIZE	(50)
#define NUM_SET		(1081/SET_SIZE)
extern double a[];
extern double b[];
extern double highlight;
void MyStat::draw()
{
	int i;
	double xpos, ypos;
    double const x0 = (w() / 2.0);
    double const y0 = (h() / 2.0);
	double rad = min(w(),h())/2.;
	double phi, d;
	double xpre, ypre;
	double ratio;
	double xx, yy;

    fl_color(FL_BLACK);
    fl_rectf(x(), y(), w(), h());

	if ( ppSlices == NULL )
		return;

	ratio = rad/((double)scale+1.0);
	slice_t *slice = ppSlices[slice_idx];

	if ( slice == NULL )
		return;

	phi = -.75*M_PI;
	d = slice->depth[0];
	xpre = x0 -  d * sin(phi)*ratio;
	ypre = y0 -  d * cos(phi)*ratio;
	int highlight_ = (int)highlight;
	int segIdx = slice->segment[highlight_];
	for ( i = 0 ; i < slice->num ; i++ )
	{
//		double phi = (double)i*1.5*M_PI/(double)slice->num - 0.75*M_PI;
		double phi = slice->phi[i];
		d = slice->depth[i];
		xx = - d * sin(phi);
		yy = - d * cos(phi);
		xpos = x0 + xx * ratio;
		ypos = y0 + yy * ratio;

		if ( slice->segment[i] == 0 )
		{
			fl_color(FL_GRAY);
		}
		else switch ( slice->segment[i] % 5 )
		{
			case 0:
//				fl_color(FL_RED);
				fl_color(FL_YELLOW);
				break;
			case 1:
				fl_color(FL_CYAN);
				break;
			case 2:
				fl_color(FL_YELLOW);
				break;
			case 3:
				fl_color(FL_BLUE);
				break;
			case 4:
				fl_color(FL_GREEN);
				break;
		}
		if ( slice->segment[i] == segIdx )
			fl_color(FL_RED);
//		fl_line(xpre, ypre, xpos, ypos);
//		fl_color(FL_WHITE);
		fl_rectf(xpos, ypos, 2., 2.);
		xpre = xpos;
		ypre = ypos;
	

#if 0
		if ( (i % SET_SIZE) == 0 && i != 0 )
		{
			double phi = slice->phi[i-SET_SIZE];
			double d = slice->depth[(i-SET_SIZE)];
			double old_x = -  d * sin(phi);
			double old_y = -  d * cos(phi);
			double a_ = a[i/SET_SIZE], b_ = b[i/SET_SIZE];
			fl_line(x0 + xx * ratio, y0 + yy*ratio, x0 + old_x*ratio, y0+(a_*(old_x) + b_)*ratio);
//			fl_line(x0 + xx * ratio, y0 + (a_*(xx) + b_)*ratio, x0 + old_x*ratio, y0+(a_*(old_x) + b_)*ratio);
//			fl_line(xpos, ypos, old_x, old_y);
			fl_color(FL_GRAY);
			fl_line(x0, y0, xpos, ypos);
		}
#endif
	}
	double a_ = slice->a[segIdx], b_ = slice->b[segIdx];
	{
		static int count = 0;

		if ( (count % 30) == 0 )
		{
			fprintf(stderr, "SEG %d, %f %f %f\n", segIdx,  a_, b_, slice->distance[segIdx]);
		}
		count++;
	}
	{
		double phi = slice->phi[highlight_];
		double d = slice->depth[highlight_];
		double old_x = -  d * sin(phi);
		double old_y = -  d * cos(phi);
		fl_line(x0, y0, x0 + old_x*ratio, y0+old_y*ratio);
	}
	if (fabs(a_) < fabs(b_) )
		fl_line(x0-100*ratio, y0 + (1. - a_*(-100.))/b_*ratio, x0 + 100*ratio, y0+(1.-a_*(100.))/b_*ratio);
	else if ( fabs(a_) > 0. )
		fl_line(x0+(1.-(-100.)*b_)/a_*ratio, y0 + -100.*ratio, x0 + (1.-100.*b_)/a_*ratio, y0+(100.)*ratio);
}

void MyStat::timer_cb(void * param)
{
	reinterpret_cast<MyStat*>(param)->redraw();

	Fl::repeat_timeout(dt, // gets initialized within tick()
			   timer_cb,
			   param);
}

int MyStat::handle(int e)
{
	int ret = 0;

	switch (e)
	{
	case FL_MOUSEWHEEL:
		fprintf(stderr, "MOUSE WHEEL!!! %f\n", scale);
		scale += Fl::event_dy()/10.;

		if ( scale < 0. )
			scale = 0.;
		ret = 1;
		break;
	}
	Fl_Widget::handle(e);
	return ret;
}

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <vector>

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include "GMM.h"

using namespace std;
using Eigen::MatrixXf;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXcd;
using Eigen::MatrixXcd;
using Eigen::EigenSolver;

double p(const VectorXd &x, const VectorXd &mean, const MatrixXd &Cov);

GMM::GMM(int n_clust, int dim)
{
	dimension = dim;

	for ( int i = 0 ; i < n_clust ; i++ )
	{
		clusters.push_back(Cluster(dim));
	}

	points = NULL;
}

GMM::~GMM(void)
{
	default_clusters.clear();
	clusters.clear();
	if ( points )
	{
		points->clear();
		delete points;
	}
}

void GMM::setSamples(vector<VectorXd> *samples)
{
	if ( points )
	{
		points->clear();
		delete points;
	}
	points = samples;
	iteration = 0;

}

void GMM::reset(void)
{
	clusters.clear();
	clusters = default_clusters;

	vector<Cluster>::iterator it;

	for ( it = clusters.begin() ; it != clusters.end() ; it++ )
	{
		it->update();
	}

	return;
}

void GMM::setDefaultClusters()
{
	default_clusters.clear();
	default_clusters = clusters;

	return;

}

double GMM::iterate(void)
{
	int i, c;
	int numSamples;
	int numClusters;
	MatrixXd ric;

	if ( !points || points->size() == 0 )
		return 0.;

	numSamples	= points->size();
	numClusters	= clusters.size();
	
	ric = MatrixXd::Zero(numSamples,numClusters);

	VectorXd mc;
	VectorXd means[numClusters];

	for ( i = 0 ; i < numClusters ; i++ )
		means[i] = clusters[i].mean;
	
	mc = VectorXd::Zero(numClusters);
	for ( i = 0 ; i < numSamples ; i++ )
	{
		double sum = 0.;
		VectorXd point = (*points)[i];
		vector<Cluster>::iterator cit;
		for ( cit = clusters.begin(), c = 0 ; cit != clusters.end() ; cit++, c++ )
		{
			ric(i,c) = cit->pi * cit->p(point);
			sum += ric(i,c);
		}
		if ( sum < 0.01 )
		{
//			fprintf(stderr, "Too small ric(%d) %f %f\n", i, ric(i,0), ric(i,1));
		}
		for ( c = 0 ; c < numClusters ; c++ )
		{
			ric(i,c) = ric(i,c) / sum;
			mc[c] += ric(i,c);
		}
	}

//	fprintf(stderr, "MC %f, %f\n", mc[0], mc[1]);

//	fprintf(stderr, "PI %f, %f\n", clusters[0].pi, clusters[1].pi);
	double sum_mc = 0;

	for ( c = 0 ; c < numClusters ; c++ )
	{
		sum_mc += mc[c];
		clusters[c].pi = mc[c] / sum_mc;
		clusters[c].mean = VectorXd::Zero(dimension);

		for ( i = 0 ; i < numSamples ; i++ )
		{
			clusters[c].mean += ric(i,c) * (*points)[i];
		}
		clusters[c].mean = clusters[c].mean / mc[c];
//		cerr << "Mean (" << c << ")"<< endl << clusters[c].mean << endl;

		clusters[c].cov = MatrixXd::Zero(dimension,dimension);
		for ( i = 0 ; i < numSamples ; i++ )
		{
			VectorXd dx;
			dx = (*points)[i] - clusters[c].mean;
			clusters[c].cov += ric(i,c) * dx * dx.transpose();
		}
		clusters[c].cov = clusters[c].cov / mc[c];
//		cerr << "Cov (" << c << ")"<< endl << clusters[c].cov << endl;

//		cerr << "Eigen " << endl << eig << endl;

		clusters[c].update();
	}
	
	max_dist2 = 0.;
	for ( i = 0 ; i < numClusters ; i++ )
	{
		double dist2 = (clusters[i].mean - means[i]).norm();

		if ( dist2 > max_dist2 )
			max_dist2 = dist2;
	}

	iteration++;

	return max_dist2;
}

double GMM::iterate(int times)
{
	int i;
	double dist2;

	for ( i = 0 ; i < times ; i++ )
	{
		dist2 = iterate();
	}

	return dist2;
}

void GMM::update(void)
{
	vector<Cluster>::iterator cit;
	for ( cit = clusters.begin() ; cit != clusters.end() ; cit++ )
	{
		cit->update();
	}
	
	return;
}

double GMM::converge(double threshold, int times)
{
	int i, n_clust, trial = 0;
	double thresh2 = threshold*threshold;

	n_clust = clusters.size();
	do
	{
		max_dist2 = iterate();

		trial++;
	}
	while ( max_dist2 > thresh2 && trial < times);

	return max_dist2;
}

Cluster::Cluster(int dim)
{
	dimension = dim;
	mean	= VectorXd::Zero(dim);
	cov		= MatrixXd::Zero(dim,dim);
	eigval	= VectorXcd::Zero(dim);
	eigvec	= MatrixXcd::Zero(dim,dim);
}

void Cluster::update(void)
{
	EigenSolver<MatrixXd> es(cov);
//	cerr << "EigenVector0 " << endl << es.eigenvectors().col(0) << endl;
//	cerr << "EigenVector1 " << endl << es.eigenvectors().col(1) << endl;
	eigval = es.eigenvalues();
	eigvec = es.eigenvectors();
	double det = cov.determinant();
	if ( det > 0. )
		sqrt_cov_det = sqrt(det);
	else
		sqrt_cov_det = -sqrt(det);
	cov_inv = cov.inverse();
	
}

double Cluster::p(const VectorXd &x)
{
	double ret;
	VectorXd X;

	if ( sqrt_cov_det <= 0. )
		return 0.;

	X = x - mean;

	double exp_value = -0.5 * (X.transpose() * cov_inv * X)(0);

	ret = 1. / (2.*M_PI*sqrt_cov_det ) * exp( exp_value );

	if (ret < 0.)
	{
		cerr << "Negative Prob " << ret << " " << sqrt_cov_det << endl;
	}

	return ret;
}

ostream& operator<<(ostream& ost, const GMM& m)
{
	ost << "===================" << endl;
	ost << "Iteration: " << m.iteration << " " << m.max_dist2 << endl;
	ost << "===================" << endl;

	return ost;
}


#if 0
double p(const VectorXd &x, const VectorXd &mean, const MatrixXd &Cov)
{
	double ret;
	VectorXd X;
	double cov_det;

	X = x - mean;
	cov_det = Cov.determinant();

	if ( cov_det <= 0. )
		return 0.;

	double exp_value = -0.5 * (X.transpose() * Cov.inverse() * X)(0);

	ret = 1. / (2.*M_PI*sqrt(cov_det) ) * exp( exp_value );

	if (ret < 0.)
	{
		cerr << "Negative Prob " << ret << " " << cov_det << endl;
	}

	return ret;
}

double getRandomNormal(void)
{
	double z0, z1;
	double u1, u2;
	double two_pi = 2*M_PI;
	do
	{
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	}
	while ( u1 <= 0.0001 );
		
	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);

	return z0;
			
}
#endif

OS = $(shell uname -s)
MACHINE = $(shell uname -m)
BUILD = $(OS)_$(MACHINE)

#cflags= -g -arch x86_64 -I /usr/local/include/eigen2\
		-Wl,-search_paths_first -Wl,-headerpad_max_install_names \
		-framework Carbon -framework Cocoa -framework ApplicationServices /usr/local/lib/libfltk.a -ltao-de
executable = objrec
target= $(BUILD)/$(executable)

SOURCES= main.cpp Octree.cpp DataStructure.cpp Group.cpp Slice.cpp PointEntry.cpp MySim.cpp HumanModel.cpp GMM.cpp KMeans.cpp
OBJECTS = $(patsubst %.cpp,$(BUILD)/%.o,$(SOURCES))

cflags= -g -I /usr/local/include/eigen3 -I /usr/include/eigen3 -I /usr/include -I ../libs $(pkg-config --cflags gl) 

ldflags= -g -Wl,-L /usr/lib -lfltk -lutils -L ../libs/$(BUILD) -L $(BUILD) -lfltk_gl -lGLU -lGL -lglut $(pkg-config --libs gl)

all: $(BUILD) $(executable) $(target)
	

$(target): $(OBJECTS)
	g++ -o $@ $^ $(ldflags)

$(OBJECTS): $(BUILD)/%.o : %.cpp Octree.h
	g++ -c $< $(cflags) -o $@

clean:
	rm -rf $(target) $(OBJECTS)

$(BUILD): 
	mkdir $(BUILD)

$(executable):
	ln -sf $(target) $@

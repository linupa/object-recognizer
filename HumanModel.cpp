#include "Octree.h"
#include "DataStructure.h"

int HumanModel::iteration = 0;
int HumanModel::creation = 0;

Point::Point(double x_, double y_, double z_)
{
	x = x_;
	y = y_;
	z = z_;
}

HumanModel::HumanModel(void)
{
}

#define NUM_CLUSTER (4)
#define NUM_DIMENSION (3)

HumanModel::HumanModel(Group &group)
{
	int i;
	double pos[3];
	double var[3];

	group.getPos(pos);
	group.getVar(var);

	if ( group.numPoint == 0 )
		return;

	vector<VectorXd> *points;
	vector<VectorXd> *hand_points;

	gmm		= new GMM(NUM_CLUSTER, NUM_DIMENSION);
	hand	= new KMeans(2, NUM_DIMENSION);

	points		= new vector<VectorXd>;
	hand_points	= new vector<VectorXd>;

	GroupEntity *entity = group.firstEntity->pNext;

	while ( entity != group.lastEntity )
	{
		VectorXd x(3);
		PointEntry *entry = entity->pPointEntry;

		x(0) = entry->x;
		x(1) = entry->y;
		x(2) = entry->z;
		points->push_back(x);

		if ( entry->contact )
			hand_points->push_back(x);

		entity = entity->pNext;
	}

	gmm->clusters[0].mean(0) = pos[0];
	gmm->clusters[0].mean(1) = pos[1];
	gmm->clusters[0].mean(2) = pos[2];

	gmm->clusters[0].cov(0,0) = var[0];
	gmm->clusters[0].cov(1,1) = var[1];
	gmm->clusters[0].cov(2,2) = var[2]/2.;


	gmm->setSamples(points);
	hand->setSamples(hand_points);
//	fprintf(stderr, "%ld\n", hand_points->size());

	creation++;
}

HumanModel::~HumanModel(void)
{
	delete gmm;
	delete hand;
}

void HumanModel::process(void)
{
	if ( hand->points->size() == 0 )
		return;

	VectorXd pos = gmm->clusters[0].mean;
	double dist = sqrt(pos[0]*pos[0] + pos[1]*pos[1]);

	gmm->clusters[0].pi = 0.3;
	gmm->clusters[1].pi = 0.2;
	gmm->clusters[2].pi = 0.2;
	gmm->clusters[3].pi = 0.1;
	gmm->clusters[1].mean		= pos;
	gmm->clusters[1].mean(2)	-= 0.1;
	gmm->clusters[2].mean(0) = pos[0] - pos[1]*0.3/dist;
	gmm->clusters[2].mean(1) = pos[1] + pos[0]*0.3/dist;
	gmm->clusters[2].mean(2) = pos[2];
	gmm->clusters[3].mean(0) = pos[0] + pos[1]*0.3/dist;
	gmm->clusters[3].mean(1) = pos[1] - pos[0]*0.3/dist;
	gmm->clusters[3].mean(2) = pos[2];
//	gmm->clusters[0].cov = 0.1*MatrixXd::Identity(3,3);
	gmm->clusters[1].cov = gmm->clusters[0].cov;
	gmm->clusters[2].cov = 0.1*gmm->clusters[0].cov;
	gmm->clusters[3].cov = 0.1*gmm->clusters[0].cov;
	gmm->clusters[0].mean(2) += gmm->clusters[0].cov(2)*2.;
	gmm->clusters[1].mean(2) -= gmm->clusters[0].cov(2)*2.;


	double max_dist = 0;
	vector<VectorXd>::iterator it = hand->points->begin();
	VectorXd x1 = *it;
	VectorXd x2 = *it;
	for ( ; it != hand->points->end() ; it++ )
	{
		double dist = (x1 - (*it)).transpose() * (x1 - (*it));
		if ( dist > max_dist )
		{
			max_dist = dist;
			x2 = *it;
		}
	}
	hand->clusters[0].mean = x1;
	hand->clusters[1].mean = x2;

#if 1
//	hand->update();
	hand->iterate(20);
#endif

	VectorXd m1, m2, d, avg;
	double dist2;
	m1 = hand->clusters[0].mean;
	m2 = hand->clusters[1].mean;
	d = m1 - m2;
	avg = (m1 + m2) / 2.;
	dist2 = d.transpose() * d;

	if ( dist2 <= 0.15*0.15 )
	{
		hand->clusters[0].mean = avg;
		gmm->clusters[2].mean = avg;
		hand->clusters.pop_back();
		gmm->clusters.pop_back();
	}
	else
	{
		gmm->clusters[2].mean = hand->clusters[0].mean;
		gmm->clusters[3].mean = hand->clusters[1].mean;
	}

#if 0
	cout<< x1 << endl;
	cout<< x2 << endl;
	cout<< hand->clusters[0].cov << endl;
	cout<< hand->clusters[1].cov << endl;
	cout<< gmm->clusters[2].mean << endl;
	cout<< gmm->clusters[3].mean << endl;
#endif

	gmm->update();
//	gmm->iterate(10);
	max_dist = gmm->converge(0.05, 1000);
//	fprintf(stderr, "Iteration %d (%f)\n", gmm->iteration,max_dist);
	iteration++;
}

ostream& operator<<(ostream& ost, const HumanModel &m)
{
	ost << "Creation/Iteration " << m.creation << " " << m.iteration << endl;
	ost << "Body CLuster " << m.gmm->clusters.size() << " " << m.gmm->points->size() << endl;
	ost << "Cluster 0 " << endl << m.gmm->clusters[0].mean << endl;
	ost << "Hand CLuster " << m.hand->clusters.size() << " " << m.hand->points->size() << endl;
	ost << "Cluster 0 " << endl << m.hand->clusters[0].mean << endl;
	ost << "Cluster 1 " << endl << m.hand->clusters[1].mean << endl;

	return ost;  
}
